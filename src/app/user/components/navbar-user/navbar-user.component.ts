import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar-user',
  templateUrl: './navbar-user.component.html',
  styleUrls: ['./navbar-user.component.css']
})
export class NavbarUserComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  
  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['/'])
    
      }
}
