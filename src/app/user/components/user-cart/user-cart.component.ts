import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
// import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-user-cart',
  templateUrl: './user-cart.component.html',
  styleUrls: ['./user-cart.component.css']
})
export class UserCartComponent implements OnInit {
  totalPayment: number;
  amnt: number;
  constructor(private products: UserService, private router: Router, private fb: FormBuilder) {
    this.rForm = new FormGroup({
      "productName": new FormControl(''),
      "productCost": new FormControl(''),
      // "totalCost": new FormControl(''),

    })
  }
  rForm: FormGroup;
  itemslist = new Array;
  items: any;
  ngOnInit() {
    this.getDetails();
  }


  getDetails() {
    var id = localStorage.getItem('id');
    // console.log('userID is ===', id)
    this.products.listCartProducts(id).subscribe(res => {
      this.itemslist = res;
      let amt = 0;
      this.itemslist.forEach((value, index) => {
        console.log("money to pay for individual product : ",this.itemslist[index].productCost)
        amt +=this.itemslist[index].productCost;
        // console.log("amnt : ", this.amnt)
      })
      this.amnt = amt;
      console.log("amnt to be paid : ", this.amnt);
      let payableAmount:number = this.amnt
      console.log('payableAmount=====',payableAmount)
      localStorage.setItem('payableAmount',(payableAmount).toString());    
      // this.itemslist.push(res);
      console.log("products", this.itemslist);
      // this.products.payment(payableAmount).subscribe(res => {
      //   if(res=200)
      //   {
      //     console.log('Success')
      //   }
      // })
    });
  }

  // calcMax () {

  //   let totalSum = 0;
  //   this.itemslist.forEach(function(items) {
  //   this.ProductTotalCost += this.items.quantity * this.items.productCost ;

  //       // totalSum += entity.quantity * entity.product.price;
  //   });
  //   this.total = totalSum;

  // }


  totalAmnt(amnt) {
    console.log("amnt : ", amnt)
    amnt = parseInt(amnt);
    this.totalPayment = 100
    console.log("totalamotpayable", this.totalPayment)
  }

  deleteCartProduct(id) {
    this.products.deleteCartProduct(id).subscribe(res => {
      console.log('Deleted product from cart');
      location.reload()
    });
  }

  counter(cart, value) {
    if (value == 'increment') {
      cart.quantity += 1;
      
    }
    if (value == 'decrement') {
      cart.quantity-= 1
    }
  }













}
