// import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../../services/user.service';
import {Component,AfterViewInit,OnDestroy,ViewChild, ElementRef, ChangeDetectorRef, OnInit} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


// // Create a Stripe client.
// var stripe = stripe('pk_test_WdE9BXUwCrlYqEGHZLW5EA9O');

// // Create an instance of Elements.
// var elements = stripe.elements();

// // Custom styling can be passed to options when creating an Element.
// // (Note that this demo uses a wider set of styles than the guide below.)
// var style = {
//   base: {
//     color: '#32325d',
//     lineHeight: '18px',
//     fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
//     fontSmoothing: 'antialiased',
//     fontSize: '16px',
//     '::placeholder': {
//       color: '#aab7c4'
//     }
//   },
//   invalid: {
//     color: '#fa755a',
//     iconColor: '#fa755a'
//   }
// };

// // Create an instance of the card Element.
// var card = elements.create('card', {style: style});

// // Add an instance of the card Element into the `card-element` <div>.
// card.mount('#card-element');

// // Handle real-time validation errors from the card Element.
// card.addEventListener('change', function(event) {
//   var displayError = document.getElementById('card-errors');
//   if (event.error) {
//     displayError.textContent = event.error.message;
//   } else {
//     displayError.textContent = '';
//   }
// });

// // Handle form submission.
// var form = document.getElementById('payment-form');
// form.addEventListener('submit', function(event) {
//   event.preventDefault();

//   stripe.createToken(card).then(function(result) {
//     if (result.error) {
//       // Inform the user if there was an error.
//       var errorElement = document.getElementById('card-errors');
//       errorElement.textContent = result.error.message;
//     } else {
//       // Send the token to your server.
//       stripeTokenHandler(result.token);
//     }
//   });
// });

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})


export class PaymentComponent implements AfterViewInit, OnDestroy {
  @ViewChild('cardInfo') cardInfo: ElementRef;

  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;

  constructor(private cd: ChangeDetectorRef, private stripePayment: UserService,
    private toastr: ToastrService,private router: Router) { }

  ngOnInit() {
  }
  
  ngAfterViewInit() {
    // const style = {
    //   base: {
    //     lineHeight: '24px',
    //     fontFamily: 'monospace',
    //     fontSmoothing: 'antialiased',
    //     fontSize: '19px',
    //     '::placeholder': {
    //       color: 'purple'
    //     }
    //   }
    // };
    this.card = elements.create('card');
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    const { token, error } = await stripe.createToken(this.card);

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      console.log('Success!', token);
      // ...send the token to the your backend to process the charge
      let payableAmount = Number(localStorage.getItem('payableAmount'))

      console.log('token.id=>',token.id,'payableAmount====',payableAmount)
      var data={
        token:token.id,
        amount:payableAmount
      }
    this.stripePayment.payment(data).subscribe(res => {
      
        if(res=200)
        {
          console.log('Success')
          this.toastr.success('Payment successfull!');
          // this.router.navigate(['/order-summary/order-summary']);
        }
      })
    }  
  }









}