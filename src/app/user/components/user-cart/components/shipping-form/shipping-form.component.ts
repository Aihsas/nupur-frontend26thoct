import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder, FormControl, Validators, NgForm } from '@angular/forms'
import { UserService } from '../../../../../services/user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-shipping-form',
  templateUrl: './shipping-form.component.html',
  styleUrls: ['./shipping-form.component.css']
})
export class ShippingFormComponent implements OnInit {
  rForm: FormGroup;
  titleAlert: string = 'This field is required';
  name: string = '';
  address: String = '';
  pincode: number;
  contact: number;
  email: string;
  emailPattern: "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  errContact: String = 'Please match pattern [+91-036-78658 || 91-036-78658]';
  errAddress: String = 'Please Enter Valid Contact number';
  errPincode: String = 'Please Enter Valid Pincode';
  errEmail: String = 'Please Enter Valid Email';
  post: any;                     // A property for our submitted form

  totalPayment: number;
  amnt: number;
  itemslist = new Array;
  items: any;

  constructor(private fb: FormBuilder, private service: UserService, private router: Router) {
    this.rForm = fb.group({
      'name': [null, Validators.required],
      'address': [null, Validators.required],
      'pincode': [null, [Validators.required, Validators.maxLength(6), Validators.pattern("^[1-9][0-9]{5}$")]],
      'contact': [null, Validators.required],
      'email': ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      'validate': '',
    })
  }

  ngOnInit() {
    this.rForm.get('validate').valueChanges.subscribe(

      (validate) => {

        if (validate == '1') {
          this.rForm.get('name').setValidators([Validators.required, Validators.minLength(3)]);
          this.titleAlert = 'You need to specify at least 3 characters';
        } else {
          this.rForm.get('name').setValidators(Validators.required);
        }
        this.rForm.get('name').updateValueAndValidity();

      });


      this.getDetails();
  }

  get officialEmail() {
    return this.rForm.get('email');
  }

  shippingForm(post: any) {

    var userId = localStorage.getItem("id");      //userId
    this.rForm.value.userId = userId;
    console.log("shipping form data",this.rForm.value)
    this.service.saveShippingDetails(post).subscribe((res: any) => {
      console.log("SHipping details sent::", res);
      this.router.navigate(['/stripe-payment/stripe-payment'], {});
    })

  }

getDetails() {
    var id = localStorage.getItem('id');
    // console.log('userID is ===', id)
    this.service.listCartProducts(id).subscribe(res => {
      this.itemslist = res;
      let amt = 0
      this.itemslist.forEach((value, index) => {
        // console.log("value : ", this.itemslist[index].quantity * this.itemslist[index].productCost)
        amt += this.itemslist[index].quantity * this.itemslist[index].productCost;
        // console.log("amnt : ", this.amnt)
      })
      this.amnt = amt;
      // console.log("amnt : ", this.amnt);
      // let payableAmount:number = this.amnt
      // console.log('payableAmount=====',payableAmount)
      // localStorage.setItem('payableAmount',(payableAmount).toString());    
      // this.itemslist.push(res);
      // console.log("products", this.itemslist);
      // this.products.payment(payableAmount).subscribe(res => {
      //   if(res=200)
      //   {
      //     console.log('Success')
      //   }
      // })
    });
  }
  deleteCartProduct(id) {
    this.service.deleteCartProduct(id).subscribe(res => {
      console.log('Deleted product from cart');
      location.reload()
    });
  }



























}
