import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
// import {CardModule} from 'primeng/card';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userClaims: any;
  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
    // this.userService.getUserClaims().subscribe((data: any) => {
    //   this.userClaims = data;

    // });
}
  Logout() {
    localStorage.removeItem('userToken');
    this.router.navigate(['/login']);
  }
}
