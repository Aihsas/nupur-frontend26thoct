import { NgModule } from '@angular/core';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { RegUserBlogsComponent } from './components/reg-user-blogs/reg-user-blogs.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { WriteBlogsComponent } from './components/reg-user-blogs/components/write-blogs/write-blogs.component';
import { BlogListingComponent } from './components/reg-user-blogs/components/blog-listing/blog-listing.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { EditBlogsComponent } from './components/reg-user-blogs/components/edit-blogs/edit-blogs.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NavbarUserComponent } from './components/navbar-user/navbar-user.component';
import { FooterUserComponent } from './components/footer-user/footer-user.component';
// import { FileUploader,FileSelectDirective } from 'ng2-file-upload';
import {CardModule} from 'primeng/card';
import {RatingModule} from 'primeng/rating';
import {PaginatorModule} from 'primeng/paginator';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import { ProductsComponent } from './components/products/products.component';
import {TableModule} from 'primeng/table';
import { UserCartComponent } from './components/user-cart/user-cart.component';
import { ShippingFormComponent } from './components/user-cart/components/shipping-form/shipping-form.component';
import { PaymentComponent } from './components/user-cart/components/payment/payment.component';
import { ChatComponent } from './components/chat/chat.component';
import { ChatService } from '../services/chat.service';


// import {EditorModule} from 'primeng/editor';




@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2SearchPipeModule,
    CardModule,
    RatingModule,
    PaginatorModule,
    ScrollPanelModule,
    TableModule
    // EditorModule,
    // FileUploader,
    // FileSelectDirective
  ],
  declarations: [DashboardComponent, 
    ContactUsComponent, 
    RegUserBlogsComponent, 
    NavbarComponent, 
    FooterComponent, 
    WriteBlogsComponent, 
    BlogListingComponent, 
    EditBlogsComponent, 
    NavbarUserComponent, 
    FooterUserComponent, 
     ProductsComponent, 
     UserCartComponent, 
     ShippingFormComponent, 
     PaymentComponent, ChatComponent,
    ],
    providers: [
      ChatService,
      {provide: LocationStrategy, useClass: HashLocationStrategy}
    ],
})
export class UserModule { }
