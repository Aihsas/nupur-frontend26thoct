import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { RegUserBlogsComponent } from './components/reg-user-blogs/reg-user-blogs.component';
import { WriteBlogsComponent } from './components/reg-user-blogs/components/write-blogs/write-blogs.component';
import { BlogListingComponent } from './components/reg-user-blogs/components/blog-listing/blog-listing.component';
import { EditBlogsComponent } from './components/reg-user-blogs/components/edit-blogs/edit-blogs.component';
import { ProductsComponent } from './components/products/products.component';
import { UserCartComponent } from './components/user-cart/user-cart.component';
import { ShippingFormComponent } from './components/user-cart/components/shipping-form/shipping-form.component';
import { PaymentComponent } from './components/user-cart/components/payment/payment.component';
import { ChatComponent } from './components/chat/chat.component';
// import { ManageUsersComponent } from '../admin/components/manage-users/manage-users.component';

const routes: Routes = [
 {
   path:'',
   component:DashboardComponent
 },
 {
   path:'contact-us',
   component:ContactUsComponent
 },
 {
   path:'reg-user-blogs',
   component:RegUserBlogsComponent
 },
 {
   path:'write-blogs',
   component:WriteBlogsComponent
 },
 {
  path:'blog-listing-user',
  component:BlogListingComponent
 },
 {
   path:'edit-blogs/:id',
   component:EditBlogsComponent
 },
 {
  path:'user-products',
  component:ProductsComponent
 },
 {
   path:'user-cart',
   component:UserCartComponent
 },
 {
   path:'shipping-form',
   component:ShippingFormComponent
 },
 {
   path:'stripe-payment',
   component:PaymentComponent
 },
 {
  path:'chat',
  component:ChatComponent
 },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UserRoutingModule { }
