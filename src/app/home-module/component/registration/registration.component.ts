import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators,NgForm} from '@angular/forms'  
import { RegistrationService } from '../../../services/registration.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  rForm: FormGroup;
  post:any;                     // A property for our submitted form
  username:string = '';      //password
  password:string = '';
  email:string;
  role:string='user';
  titleAlert:string = 'This field is required';
  // ServicesService:any;
  emailPattern : "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

  constructor(private fb: FormBuilder,private service:RegistrationService,private router: Router) 
{ 
  this.rForm = fb.group({
  'username' : [null, Validators.required],
  'password' : [null, Validators.required],
  'validate' : '',
  'email': ['', [Validators.required, Validators.pattern(this.emailPattern)]],
  'role':'user',
});

}
ngOnInit() {
  this.rForm.get('validate').valueChanges.subscribe(

    (validate) => {

        if (validate == '1') {
            this.rForm.get('username').setValidators([Validators.required, Validators.minLength(3)]);
            this.titleAlert = 'You need to specify at least 3 characters';
        } else {
            this.rForm.get('username').setValidators(Validators.required);
        }
        this.rForm.get('username').updateValueAndValidity();

    });
}
addPost(post:any) {
  this.service.addPost(post).subscribe((res:any)=>{
    console.log("You are registered!::",res);
    console.log('res====',res)
    localStorage.setItem('LoggedInUser', 'true');    
    localStorage.setItem('id',res.data._id)
    localStorage.setItem('token', res.data.token);    
    localStorage.setItem("role",res.data.role);
    this.router.navigate(['/userdashboard']);
  })

}
  get officialEmail() {
    return this.rForm.get('email');
} 
}
