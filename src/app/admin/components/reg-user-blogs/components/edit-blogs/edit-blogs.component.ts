import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { UserService } from '../../../../../services/user.service';
import { ReactiveFormsModule } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-blogs',
  templateUrl: './edit-blogs.component.html',
  styleUrls: ['./edit-blogs.component.css']
})
export class EditBlogsComponent implements OnInit {
  rForm: FormGroup;
  post:any;                     // A property for our submitted form
  description:string = '';
  name:string = '';   
  titleAlert:string = 'This field is required';
  file :null;

  @ViewChild('fileInput') fileInput: ElementRef;


  constructor(private fb: FormBuilder,private route:ActivatedRoute,
    private service:UserService, private router: Router,) {
      this.rForm = fb.group({
        'name' : [null, Validators.required],
        'description' : [null, Validators.compose([Validators.required, Validators.minLength(30), Validators.maxLength(9000)])],
        'validate' : '',
        file :null
      });
     }

  ngOnInit() {

     this.rForm.get('validate').valueChanges.subscribe(

      (validate) => {

          if (validate == '1') {
              this.rForm.get('name').setValidators([Validators.required, Validators.minLength(3)]);
              this.titleAlert = 'You need to specify at least 3 characters';
          } else {
              this.rForm.get('name').setValidators(Validators.required);
          }
          this.rForm.get('name').updateValueAndValidity();

      });
  }

  onFileChange(event) {
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
      this.rForm.get('file').setValue(file);
    }
  }
  
  private prepareSave(): any {
    let input = new FormData();
    input.append('name', this.rForm.get('name').value);
    input.append('description', this.rForm.get('description').value);
    input.append('file', this.rForm.get('file').value);
    return input;
  }

editBlogs(data, id){
  const formModel = this.prepareSave();
  this.route.params.subscribe(params =>{
    this.service.editBlogs(params.id,formModel).subscribe((res:any)=>{
      console.log("Edit your blog!::",res)
      if(res.code=200){
        alert(res)
      this.router.navigate['/blogs-listing/blogs-listing']

      }
    })
  })
}



// getAutoValutionReportById(id){
//   if(id){
//     this.service
//     .getAutoValutionReportById(id)
//     .subscribe(res=>{
//       console.log(res)
//       if(res['code'] == 200){
//         this.suburbForm.patchValue({
//           zipcode: res['data'].zipcode,
//           address: res['data'].address,
//           city: res['data'].city,
//           state: res['data'].state,
//           pdf: StringConst.awspath + res['data'].filepath+ '/'+ res['data'].filename    //image
//         })
//       }
//     })
//   }
// }

}
