import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { HomeModuleModule } from './home-module/home-module.module';
import { HomepageComponent } from './home-module/component/homepage/homepage.component';
// import {ReactiveFormsModule} from '@angular/forms'  
// import { RegistrationComponent } from './home-module/component/registration/registration.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthServicesService } from './services/auth-services.service';
import { AuthGuard } from './guard/auth.guard';
import { LoginService } from './services/login.service';
import { RegistrationService } from './services/registration.service';
import {FormsModule} from '@angular/forms';
// import { UserManageComponent } from './admin/user-manage/user-manage.component';
// import { DashboardComponent } from '../app/user/components/dashboard/dashboard.component';
// import { FormsModule } from '@angular/forms';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
// import { HashLocationStrategy, LocationStrategy } from '@angular/common';

// import { ToastrModule } from 'ngx-toastr';
@NgModule({
  declarations: [
    AppComponent,
    // UserManageComponent,
    // DashboardComponent,
    // HomepageComponent
    // RegistrationComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModuleModule,
    HttpClientModule,
    FormsModule,
    HttpModule,
    // ReactiveFormsModule,
    // FormsModule,
    //BrowserAnimationsModule, // required animations module
    //ToastrModule.forRoot(),
    RouterModule.forRoot([])
  ],
  providers: [AuthServicesService,AuthGuard,LoginService,RegistrationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
