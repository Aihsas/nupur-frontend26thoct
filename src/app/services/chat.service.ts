// import { Injectable } from '@angular/core';
// import { Http, Headers } from '@angular/http';
// // import 'rxjs/add/operator/map';  
// import {Observable} from 'rxjs'
import { HttpClient} from '@angular/common/http';
// import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
// import 'rxjs/add/operator';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private http: Http) { }

  getChatByRoom(room) {
    console.log("room==",room);
    let header = new Headers();
    let option: any = {
      headers: header
    }
    return new Promise((resolve, reject) => {
      this.http.get('http://localhost:10010/authentication/chat/'+ room, option).pipe(
        map(res => res.json()))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        })
    });
  }

  showChat(id) {
    return new Promise((resolve, reject) => {
        this.http.get('/chat/' + id).pipe(
          map(res => res.json()))
          .subscribe(res => {
            resolve(res)
        }, (err) => {
          reject(err);
        });
    });
  }

  saveChat(data) {
    return new Promise((resolve, reject) => {
        this.http.post('/chat', data).pipe(
          map(res => res.json()))
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
    });
  }

  updateChat(id, data) {
    return new Promise((resolve, reject) => {
        this.http.put('/chat/'+id, data).pipe(
          map(res => res.json()))
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
    });
  }

  deleteChat(id) {
    return new Promise((resolve, reject) => {
        this.http.delete('/chat/'+id)
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
    });
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
