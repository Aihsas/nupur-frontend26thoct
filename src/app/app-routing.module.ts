import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './home-module/component/homepage/homepage.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistrationComponent } from './home-module/component/registration/registration.component';
import { LoginComponent } from './home-module/component/login/login.component';
import { DashboardComponent } from './user/components/dashboard/dashboard.component';
import { BlogsComponent } from './home-module/component/blogs/blogs.component';
import { AuthServicesService } from './services/auth-services.service';
import { AuthGuard } from './guard/auth.guard';



const routes: Routes = [
  {
    path:'',
    component: HomepageComponent 
  },
  {
    path:'signup',
    component:RegistrationComponent
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'blogs',
    component:BlogsComponent
  },
  {
    path: 'dashboard',
    loadChildren:'./admin/admin.module#AdminModule',
    canActivate: [AuthGuard] ,
  },
  {
    path:'contact-us',
    loadChildren:'./admin/admin.module#AdminModule',
    canActivate:[AuthGuard]
  },
  {
    path: 'reg-user-blogs',
    loadChildren:'./admin/admin.module#AdminModule',
    canActivate: [AuthGuard] ,
  },
  {
    path: 'write-blogs',
    loadChildren:'./admin/admin.module#AdminModule',
    canActivate: [AuthGuard] ,
  },
  {
    path: 'blogs-listing',
    loadChildren:'./admin/admin.module#AdminModule',
    canActivate: [AuthGuard] ,
  },
  {
    path: 'edit-blogs',
    loadChildren:'./admin/admin.module#AdminModule',
    canActivate: [AuthGuard] ,
  },
  {
      path: 'manage-user',
      loadChildren:'./admin/admin.module#AdminModule',
      canActivate: [AuthGuard] , 
  },
  {
    path:'manage-users',
    loadChildren:'./admin/admin.module#AdminModule',
    canActivate: [AuthGuard] , 
  },
  {
    path:'',
    loadChildren:'./admin/admin.module#AdminModule',
    canActivate: [AuthGuard]
  },
  {
    path:'manage-products',
    loadChildren:'./admin/admin.module#AdminModule',
    canActivate: [AuthGuard]
  },
  {
    path:'userdashboard',
    loadChildren:'./user/user.module#UserModule',
    canActivate: [AuthGuard]
  },
  {
    path:'blog-listing-user',
    loadChildren:'./user/user.module#UserModule',
    canActivate: [AuthGuard]
  },
  {
    path:'user-products',
    loadChildren:'./user/user.module#UserModule',
    canActivate: [AuthGuard]
  },
  {
    path:'user-cart',
    loadChildren:'./user/user.module#UserModule',
    canActivate: [AuthGuard]
  },
  {
    path:'shipping-form',
    loadChildren:'./user/user.module#UserModule',
    canActivate: [AuthGuard]
  },
  {
    path:'stripe-payment',
    loadChildren:'./user/user.module#UserModule',
    canActivate: [AuthGuard]
  },
  {
    path:'authenticate',
    loadChildren:'./user/user.module#UserModule',
    canActivate: [AuthGuard]
  },
]

@NgModule({
  imports: [
    [RouterModule.forRoot(routes)],
    ReactiveFormsModule

  ],
  exports:[RouterModule],
  declarations: []
})
export class AppRoutingModule { }
