(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./admin/admin.module": [
		"./src/app/admin/admin.module.ts",
		"default~admin-admin-module~user-user-module",
		"admin-admin-module"
	],
	"./user/user.module": [
		"./src/app/user/user.module.ts",
		"default~admin-admin-module~user-user-module",
		"user-user-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_module_component_homepage_homepage_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home-module/component/homepage/homepage.component */ "./src/app/home-module/component/homepage/homepage.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _home_module_component_registration_registration_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home-module/component/registration/registration.component */ "./src/app/home-module/component/registration/registration.component.ts");
/* harmony import */ var _home_module_component_login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-module/component/login/login.component */ "./src/app/home-module/component/login/login.component.ts");
/* harmony import */ var _home_module_component_blogs_blogs_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-module/component/blogs/blogs.component */ "./src/app/home-module/component/blogs/blogs.component.ts");
/* harmony import */ var _guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./guard/auth.guard */ "./src/app/guard/auth.guard.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// import { CommonModule } from '@angular/common';







var routes = [
    {
        path: '',
        component: _home_module_component_homepage_homepage_component__WEBPACK_IMPORTED_MODULE_2__["HomepageComponent"]
    },
    {
        path: 'signup',
        component: _home_module_component_registration_registration_component__WEBPACK_IMPORTED_MODULE_4__["RegistrationComponent"]
    },
    {
        path: 'login',
        component: _home_module_component_login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"]
    },
    {
        path: 'blogs',
        component: _home_module_component_blogs_blogs_component__WEBPACK_IMPORTED_MODULE_6__["BlogsComponent"]
    },
    {
        path: 'dashboard',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]],
    },
    {
        path: 'contact-us',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: 'reg-user-blogs',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]],
    },
    {
        path: 'write-blogs',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]],
    },
    {
        path: 'blogs-listing',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]],
    },
    {
        path: 'edit-blogs',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]],
    },
    {
        path: 'manage-user',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]],
    },
    {
        path: 'manage-users',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]],
    },
    {
        path: '',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: 'manage-products',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: 'userdashboard',
        loadChildren: './user/user.module#UserModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: 'blog-listing-user',
        loadChildren: './user/user.module#UserModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: 'user-products',
        loadChildren: './user/user.module#UserModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: 'user-cart',
        loadChildren: './user/user.module#UserModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: 'shipping-form',
        loadChildren: './user/user.module#UserModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: 'stripe-payment',
        loadChildren: './user/user.module#UserModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
    {
        path: 'authenticate',
        loadChildren: './user/user.module#UserModule',
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"]]
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
            declarations: []
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'frontend';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_module_home_module_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-module/home-module.module */ "./src/app/home-module/home-module.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_auth_services_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/auth-services.service */ "./src/app/services/auth-services.service.ts");
/* harmony import */ var _guard_auth_guard__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./guard/auth.guard */ "./src/app/guard/auth.guard.ts");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _services_registration_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/registration.service */ "./src/app/services/registration.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// import {ReactiveFormsModule} from '@angular/forms'  
// import { RegistrationComponent } from './home-module/component/registration/registration.component';






// import { UserManageComponent } from './admin/user-manage/user-manage.component';
// import { DashboardComponent } from '../app/user/components/dashboard/dashboard.component';
// import { FormsModule } from '@angular/forms';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import { HashLocationStrategy, LocationStrategy } from '@angular/common';
// import { ToastrModule } from 'ngx-toastr';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _home_module_home_module_module__WEBPACK_IMPORTED_MODULE_5__["HomeModuleModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_12__["HttpModule"],
                // ReactiveFormsModule,
                // FormsModule,
                //BrowserAnimationsModule, // required animations module
                //ToastrModule.forRoot(),
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot([])
            ],
            providers: [_services_auth_services_service__WEBPACK_IMPORTED_MODULE_7__["AuthServicesService"], _guard_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"], _services_login_service__WEBPACK_IMPORTED_MODULE_9__["LoginService"], _services_registration_service__WEBPACK_IMPORTED_MODULE_10__["RegistrationService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/guard/auth.guard.ts":
/*!*************************************!*\
  !*** ./src/app/guard/auth.guard.ts ***!
  \*************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth-services.service */ "./src/app/services/auth-services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        if (this.auth.isLoggednIn()) {
            return true;
        }
        else {
            this.router.navigate([""]);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_services_auth_services_service__WEBPACK_IMPORTED_MODULE_2__["AuthServicesService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/home-module/component/blogs/blogs.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/home-module/component/blogs/blogs.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/home-module/component/blogs/blogs.component.html":
/*!******************************************************************!*\
  !*** ./src/app/home-module/component/blogs/blogs.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<html>\n<head>\n<link href=\"//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">\n<script src=\"//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js\"></script>\n<script src=\"//code.jquery.com/jquery-1.11.1.min.js\"></script>\n<link href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">\n  <script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js\"></script>\n  <script src=\"//code.jquery.com/jquery-1.11.1.min.js\"></script>\n</head>\n<!------ Include the above in your HEAD tag ---------->\n<body>\n    <nav class=\"navbar navbar-default\">\n        <div class=\"container\">\n          <!-- Brand and toggle get grouped for better mobile display -->\n          <div class=\"navbar-header\">\n            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar-collapse-2\">\n              <span class=\"sr-only\">Toggle navigation</span>\n              <span class=\"icon-bar\"></span>\n              <span class=\"icon-bar\"></span>\n              <span class=\"icon-bar\"></span>\n            </button>\n            <a class=\"navbar-brand\" href=\"#\">EcommerceOne</a>\n          </div>\n      \n          <!-- Collect the nav links, forms, and other content for toggling -->\n          <div class=\"collapse navbar-collapse\" id=\"navbar-collapse-2\">\n            <ul class=\"nav navbar-nav navbar-right\">\n              <li><a class=\"active\" href=\"#home\">Home</a></li>\n              <!-- <li><a class=\"active\">Home</a></li> -->\n              <li><a routerLink ='/blogs'>Blogs</a></li>\n              <!-- <li><a routerLink ='/products'>Our Products</a></li> -->\n              <li><a routerLink ='/login'>Login</a></li>\n              <li><a routerLink ='/signup'>Signup</a></li>\n            </ul>\n      <!--         \n              <li><a href=\"#\">Services</a></li>\n              <li><a href=\"#\">Works</a></li>\n              <li><a href=\"#\">News</a></li>\n              <li><a href=\"#\">Contact</a></li> -->\n              <!-- <li>\n                <a class=\"btn btn-default btn-outline btn-circle collapsed\"  data-toggle=\"collapse\" href=\"#nav-collapse2\" aria-expanded=\"false\" aria-controls=\"nav-collapse2\">Sign in</a>\n              </li>\n            </ul>\n            <div class=\"collapse nav navbar-nav nav-collapse slide-down\" id=\"nav-collapse2\">\n              <form class=\"navbar-form navbar-right form-inline\" role=\"form\">\n                <div class=\"form-group\">\n                  <label class=\"sr-only\" for=\"Email\">Email</label>\n                  <input type=\"email\" class=\"form-control\" id=\"Email\" placeholder=\"Email\" autofocus required />\n                </div>\n                <div class=\"form-group\">\n                  <label class=\"sr-only\" for=\"Password\">Password</label>\n                  <input type=\"password\" class=\"form-control\" id=\"Password\" placeholder=\"Password\" required />\n                </div>\n                <button type=\"submit\" class=\"btn btn-success\">Sign in</button> \n              </form>-->\n            \n          </div><!-- /.navbar-collapse -->\n        </div><!-- /.container -->\n      </nav><!-- /.navbar -->\n<link rel=\"stylesheet\" href=\"http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css\"/>\n<div class=\"container\">\n  <div class=\"well\">\n      <div class=\"media\">\n      \t<a class=\"pull-left\" href=\"#\">\n    \t\t<img class=\"media-object\" src=\"../../../../assets/images/itachi.png\">\n  \t\t</a>\n  \t\t<div class=\"media-body\">\n    \t\t<h4 class=\"media-heading\">Receta 1</h4>\n          <p class=\"text-right\">By Francisco</p>\n          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. \nQuisque mauris augue, molestie tincidunt condimentum vitae, gravida a libero. Aenean sit amet felis \ndolor, in sagittis nisi. Sed ac orci quis tortor imperdiet venenatis. Duis elementum auctor accumsan. \nAliquam in felis sit amet augue.</p>\n          <ul class=\"list-inline list-unstyled\">\n  \t\t\t<li><span><i class=\"glyphicon glyphicon-calendar\"></i> 2 days, 8 hours </span></li>\n            <li>|</li>\n            <span><i class=\"glyphicon glyphicon-comment\"></i> 2 comments</span>\n            <li>|</li>\n            <li>\n               <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star-empty\"></span>\n            </li>\n            <li>|</li>\n            <li>\n            <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->\n              <span><i class=\"fa fa-facebook-square\"></i></span>\n              <span><i class=\"fa fa-twitter-square\"></i></span>\n              <span><i class=\"fa fa-google-plus-square\"></i></span>\n            </li>\n\t\t\t</ul>\n       </div>\n    </div>\n  </div>\n   <div class=\"well\">\n      <div class=\"media\">\n  \t\t<a class=\"pull-left\" href=\"#\">\n    \t\t<img class=\"media-object\" src=\"../../../../assets/images/rinnegan.jpeg\">\n  \t\t</a>\n  \t\t<div class=\"media-body\">\n    \t\t<h4 class=\"media-heading\">Receta 2</h4>\n          <p class=\"text-right\">By Anailuj</p>\n          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. \nQuisque mauris augue, molestie tincidunt condimentum vitae, gravida a libero. Aenean sit amet felis \ndolor, in sagittis nisi. Sed ac orci quis tortor imperdiet venenatis. Duis elementum auctor accumsan. \nAliquam in felis sit amet augue.</p>\n          <ul class=\"list-inline list-unstyled\">\n  \t\t\t<li><span><i class=\"glyphicon glyphicon-calendar\"></i> 2 days, 8 hours </span></li>\n            <li>|</li>\n            <span><i class=\"glyphicon glyphicon-comment\"></i> 2 comments</span>\n            <li>|</li>\n            <li>\n               <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star-empty\"></span>\n            </li>\n            <li>|</li>\n            <li>\n            <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->\n              <span><i class=\"fa fa-facebook-square\"></i></span>\n              <span><i class=\"fa fa-twitter-square\"></i></span>\n              <span><i class=\"fa fa-google-plus-square\"></i></span>\n            </li>\n\t\t\t</ul>\n       </div>\n    </div>\n  </div>\n  <div class=\"well\">\n      <div class=\"media\">\n      \t<a class=\"pull-left\" href=\"#\">\n    \t\t<img class=\"media-object\" src=\"../../../../assets/images/madara.jpeg\">\n  \t\t</a>\n  \t\t<div class=\"media-body\">\n    \t\t<h4 class=\"media-heading\">Receta 1</h4>\n          <p class=\"text-right\">By Francisco</p>\n          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. \nQuisque mauris augue, molestie tincidunt condimentum vitae, gravida a libero. Aenean sit amet felis \ndolor, in sagittis nisi. Sed ac orci quis tortor imperdiet venenatis. Duis elementum auctor accumsan. \nAliquam in felis sit amet augue.</p>\n          <ul class=\"list-inline list-unstyled\">\n  \t\t\t<li><span><i class=\"glyphicon glyphicon-calendar\"></i> 2 days, 8 hours </span></li>\n            <li>|</li>\n            <span><i class=\"glyphicon glyphicon-comment\"></i> 2 comments</span>\n            <li>|</li>\n            <li>\n               <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star-empty\"></span>\n            </li>\n            <li>|</li>\n            <li>\n            <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->\n              <span><i class=\"fa fa-facebook-square\"></i></span>\n              <span><i class=\"fa fa-twitter-square\"></i></span>\n              <span><i class=\"fa fa-google-plus-square\"></i></span>\n            </li>\n\t\t\t</ul>\n       </div>\n    </div>\n  </div>\n  <div class=\"well\">\n      <div class=\"media\">\n      \t<a class=\"pull-left\" href=\"#\">\n    \t\t<img class=\"media-object\" src=\"../../../../assets/images/naruto.jpeg\">\n  \t\t</a>\n  \t\t<div class=\"media-body\">\n    \t\t<h4 class=\"media-heading\">Receta 1</h4>\n          <p class=\"text-right\">By Francisco</p>\n          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. \nQuisque mauris augue, molestie tincidunt condimentum vitae, gravida a libero. Aenean sit amet felis \ndolor, in sagittis nisi. Sed ac orci quis tortor imperdiet venenatis. Duis elementum auctor accumsan. \nAliquam in felis sit amet augue.</p>\n          <ul class=\"list-inline list-unstyled\">\n  \t\t\t<li><span><i class=\"glyphicon glyphicon-calendar\"></i> 2 days, 8 hours </span></li>\n            <li>|</li>\n            <span><i class=\"glyphicon glyphicon-comment\"></i> 2 comments</span>\n            <li>|</li>\n            <li>\n               <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star\"></span>\n                        <span class=\"glyphicon glyphicon-star-empty\"></span>\n            </li>\n            <li>|</li>\n            <li>\n            <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->\n              <span><i class=\"fa fa-facebook-square\"></i></span>\n              <span><i class=\"fa fa-twitter-square\"></i></span>\n              <span><i class=\"fa fa-google-plus-square\"></i></span>\n            </li>\n\t\t\t</ul>\n       </div>\n    </div>\n  </div>\n</div>\n</body>\n<html>\n"

/***/ }),

/***/ "./src/app/home-module/component/blogs/blogs.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/home-module/component/blogs/blogs.component.ts ***!
  \****************************************************************/
/*! exports provided: BlogsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogsComponent", function() { return BlogsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BlogsComponent = /** @class */ (function () {
    function BlogsComponent() {
    }
    BlogsComponent.prototype.ngOnInit = function () {
    };
    BlogsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-blogs',
            template: __webpack_require__(/*! ./blogs.component.html */ "./src/app/home-module/component/blogs/blogs.component.html"),
            styles: [__webpack_require__(/*! ./blogs.component.css */ "./src/app/home-module/component/blogs/blogs.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], BlogsComponent);
    return BlogsComponent;
}());



/***/ }),

/***/ "./src/app/home-module/component/homepage/homepage.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/home-module/component/homepage/homepage.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".navbar-brand { position: relative; z-index: 2; }\n\n.navbar-nav.navbar-right .btn { position: relative; z-index: 2; padding: 4px 20px; margin: 10px auto; transition: -webkit-transform 0.3s; transition: transform 0.3s; transition: transform 0.3s, -webkit-transform 0.3s; }\n\n.navbar .navbar-collapse { position: relative; overflow: hidden !important; }\n\n.navbar .navbar-collapse .navbar-right > li:last-child { padding-left: 22px; }\n\n.navbar .nav-collapse { position: absolute; z-index: 1; top: 0; left: 0; right: 0; bottom: 0; margin: 0; padding-right: 120px; padding-left: 80px; width: 100%; }\n\n.navbar.navbar-default .nav-collapse { background-color: #f8f8f8; }\n\n.navbar.navbar-inverse .nav-collapse { background-color: #222; }\n\n.navbar .nav-collapse .navbar-form { border-width: 0; box-shadow: none; }\n\n.nav-collapse>li { float: right; }\n\n.btn.btn-circle { border-radius: 50px; }\n\n.btn.btn-outline { background-color: transparent; }\n\n.navbar-nav.navbar-right .btn:not(.collapsed) {\n    background-color: rgb(111, 84, 153);\n    border-color: rgb(111, 84, 153);\n    color: rgb(255, 255, 255);\n}\n\n.navbar.navbar-default .nav-collapse,\n.navbar.navbar-inverse .nav-collapse {\n    height: auto !important;\n    transition: -webkit-transform 0.3s;\n    transition: transform 0.3s;\n    transition: transform 0.3s, -webkit-transform 0.3s;\n    -webkit-transform: translate(0px,-50px);\n            transform: translate(0px,-50px);\n}\n\n.navbar.navbar-default .nav-collapse.in,\n.navbar.navbar-inverse .nav-collapse.in {\n    -webkit-transform: translate(0px,0px);\n            transform: translate(0px,0px);\n}\n\n@media screen and (max-width: 767px) {\n    .navbar .navbar-collapse .navbar-right > li:last-child { padding-left: 15px; padding-right: 15px; } \n    \n    .navbar .nav-collapse { margin: 7.5px auto; padding: 0; }\n    .navbar .nav-collapse .navbar-form { margin: 0; }\n    .nav-collapse>li { float: none; }\n    \n    .navbar.navbar-default .nav-collapse,\n    .navbar.navbar-inverse .nav-collapse {\n        -webkit-transform: translate(-100%,0px);\n                transform: translate(-100%,0px);\n    }\n    .navbar.navbar-default .nav-collapse.in,\n    .navbar.navbar-inverse .nav-collapse.in {\n        -webkit-transform: translate(0px,0px);\n                transform: translate(0px,0px);\n    }\n    \n    .navbar.navbar-default .nav-collapse.slide-down,\n    .navbar.navbar-inverse .nav-collapse.slide-down {\n        -webkit-transform: translate(0px,-100%);\n                transform: translate(0px,-100%);\n    }\n    .navbar.navbar-default .nav-collapse.in.slide-down,\n    .navbar.navbar-inverse .nav-collapse.in.slide-down {\n        -webkit-transform: translate(0px,0px);\n                transform: translate(0px,0px);\n    }\n}\n\n.img.img{\n    overflow: hidden;\n    height: 10%;\n}\n\n/* .row.content.h1{\n    color: white;\n    position: absolute;\n    bottom: 30px;\n    right: 16px;\n} */\n\n/* .row {\n    position: relative;\n    text-align: right;\n} */\n\n/* .text h1{\n    color: red\n}  */\n\n.content h1{\n    position: relative;\n    text-align: right;\n    margin-right: 70px;\n    margin-top: -15%;\n    color: white\n}\n\n.content h2{   \n        position: relative;\n        text-align: right;\n        margin-right: 70px;\n        margin-top: 0px;\n        color: white\n}\n"

/***/ }),

/***/ "./src/app/home-module/component/homepage/homepage.component.html":
/*!************************************************************************!*\
  !*** ./src/app/home-module/component/homepage/homepage.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<html>\n  <link href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">\n  <script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js\"></script>\n  <script src=\"//code.jquery.com/jquery-1.11.1.min.js\"></script>\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n  \n  <body>\n  <nav class=\"navbar navbar-default\">\n    <div class=\"container\">\n      <!-- Brand and toggle get grouped for better mobile display -->\n      <div class=\"navbar-header\">\n        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar-collapse-2\">\n          <span class=\"sr-only\">Toggle navigation</span>\n          <span class=\"icon-bar\"></span>\n          <span class=\"icon-bar\"></span>\n          <span class=\"icon-bar\"></span>\n        </button>\n        <a class=\"navbar-brand\" href=\"#\">EcommerceOne</a>\n      </div>\n  \n      <!-- Collect the nav links, forms, and other content for toggling -->\n      <div class=\"collapse navbar-collapse\" id=\"navbar-collapse-2\">\n        <ul class=\"nav navbar-nav navbar-right\">\n          <li><a class=\"active\" href=\"#home\">Home</a></li>\n          <!-- <li><a class=\"active\">Home</a></li> -->\n          <li><a routerLink ='/blogs'>Blogs</a></li>\n          <!-- <li><a routerLink ='/products'>Our Products</a></li> -->\n          <li><a routerLink ='/login'>Login</a></li>\n          <li><a routerLink ='/signup'>Signup</a></li>\n        </ul>\n  <!--         \n          <li><a href=\"#\">Services</a></li>\n          <li><a href=\"#\">Works</a></li>\n          <li><a href=\"#\">News</a></li>\n          <li><a href=\"#\">Contact</a></li> -->\n          <!-- <li>\n            <a class=\"btn btn-default btn-outline btn-circle collapsed\"  data-toggle=\"collapse\" href=\"#nav-collapse2\" aria-expanded=\"false\" aria-controls=\"nav-collapse2\">Sign in</a>\n          </li>\n        </ul>\n        <div class=\"collapse nav navbar-nav nav-collapse slide-down\" id=\"nav-collapse2\">\n          <form class=\"navbar-form navbar-right form-inline\" role=\"form\">\n            <div class=\"form-group\">\n              <label class=\"sr-only\" for=\"Email\">Email</label>\n              <input type=\"email\" class=\"form-control\" id=\"Email\" placeholder=\"Email\" autofocus required />\n            </div>\n            <div class=\"form-group\">\n              <label class=\"sr-only\" for=\"Password\">Password</label>\n              <input type=\"password\" class=\"form-control\" id=\"Password\" placeholder=\"Password\" required />\n            </div>\n            <button type=\"submit\" class=\"btn btn-success\">Sign in</button> \n          </form>-->\n        \n      </div><!-- /.navbar-collapse -->\n    </div><!-- /.container -->\n  </nav><!-- /.navbar -->\n  <div class=\"row\">\n    <div class=\"content\">\n      <div class=\"img\">\n        <img src=\"../assets/images/ecommerceedited.jpg\">\n      </div>\n      <!-- <div class=\"text\"> -->\n        <h1>Life made easy.</h1>\n        <h2>Easy life sorted.<br>\n        Blog,Shop and chill.</h2>\n      <!-- </div> -->\n    </div>\n  </div>\n  </body>\n  </html>\n  \n  <router-outlet></router-outlet>\n  "

/***/ }),

/***/ "./src/app/home-module/component/homepage/homepage.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/home-module/component/homepage/homepage.component.ts ***!
  \**********************************************************************/
/*! exports provided: HomepageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageComponent", function() { return HomepageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomepageComponent = /** @class */ (function () {
    function HomepageComponent() {
    }
    HomepageComponent.prototype.ngOnInit = function () {
    };
    HomepageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-homepage',
            template: __webpack_require__(/*! ./homepage.component.html */ "./src/app/home-module/component/homepage/homepage.component.html"),
            styles: [__webpack_require__(/*! ./homepage.component.css */ "./src/app/home-module/component/homepage/homepage.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomepageComponent);
    return HomepageComponent;
}());



/***/ }),

/***/ "./src/app/home-module/component/login/login.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/home-module/component/login/login.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body{\n    background-image: url('videoblocks-bitcoins-rain-ecommerce-animation-illustration-design_sblpxd45g_thumbnail-full01.png');\n    background-size: cover;\n}\n\n@media screen and (min-width: 600px) {  \n    .center-login {\n      width: 600px;\n      margin: 0 auto;\n      height: 650px;\n    }\n  }"

/***/ }),

/***/ "./src/app/home-module/component/login/login.component.html":
/*!******************************************************************!*\
  !*** ./src/app/home-module/component/login/login.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<html>\n  <head>\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.min.css\">\n</head>\n<body>\n    <div class=\"ui-lg-12 ui-md-12 ui-sm-12 center-login\">\n\n<p-card [style]=\"{width: '600px'}\">\n\n<div *ngIf=\"!name; else forminfo\">\n  <form [formGroup]=\"rForm\" (ngSubmit)=\"login(rForm.value)\">\n    <div class=\"form-container\">\n      <div class=\"row columns\">\n       \n        <h1 style=\"text-align: center\">Login Portal</h1>\n        <label>Username\n          <input type=\"text\" formControlName=\"username\" class=\"form-control\" style=\"width:530px\">\n        </label>\n        <div class=\"alert\" *ngIf=\"!rForm.controls['username'].valid && rForm.controls['username'].touched\">{{ titleAlert }}</div>\n\n        <label>Password\n          <input type=\"password\" formControlName=\"password\" class=\"form-control\" style=\"width:530px\">\n        </label>\n       <input type=\"submit\" class=\"button\" value=\"Login\" [disabled]=\"!rForm.valid\" style=\"width:530px\">\n      \n    \n      </div>\n    </div>\n  </form>\n\n</div>\n</p-card>\n</div>\n</body>\n</html>\n\n"

/***/ }),

/***/ "./src/app/home-module/component/login/login.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/home-module/component/login/login.component.ts ***!
  \****************************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import {CardModule} from 'primeng/card';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(fb, service, router, toastr) {
        this.fb = fb;
        this.service = service;
        this.router = router;
        this.toastr = toastr;
        this.username = ''; //username
        this.password = ''; //password
        this.showLoginWarningMessage = false;
        this.rForm = fb.group({
            'username': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'validate': '',
            'email': ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.emailPattern)]]
        });
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.rForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            'username': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"],
            'password': new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"] //password
        });
    };
    LoginComponent.prototype.login = function (post) {
        var _this = this;
        this.service.login(post).subscribe(function (res) {
            console.log("loginservice::", res);
            // var token = res.token;
            if (res.code == 200) {
                localStorage.setItem('LoggedInUser', 'true');
                localStorage.setItem('id', res.user._id);
                localStorage.setItem('token', res.user.token);
                localStorage.setItem("role", res.user.role);
                // if(localStorage.getItem()=="admin")
                if (res.user.role == "admin") {
                    _this.router.navigate(['/dashboard']);
                    _this.toastr.success('admin login successfully');
                    alert(res.message);
                }
                else {
                    _this.router.navigate(['/userdashboard']);
                    alert(res.message);
                }
            }
            else {
                alert(res.message);
            }
            // console.log('token is: ',token)
        });
    };
    Object.defineProperty(LoginComponent.prototype, "officialEmail", {
        get: function () {
            return this.rForm.get('email');
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/home-module/component/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/home-module/component/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/home-module/component/registration/registration.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/home-module/component/registration/registration.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/home-module/component/registration/registration.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/home-module/component/registration/registration.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.min.css\">\n\n<div *ngIf=\"!name; else forminfo\">\n  <form [formGroup]=\"rForm\" (ngSubmit)=\"addPost(rForm.value)\">\n    <div class=\"form-container\">\n      <div class=\"row columns\">\n        <h1>Register</h1>\n        <label>Username\n          <input type=\"text\" formControlName=\"username\">\n        </label>\n        <div class=\"alert\" *ngIf=\"!rForm.controls['username'].valid && rForm.controls['username'].touched\">{{ titleAlert }}</div>\n\n        <label>Password\n          <input type=\"password\" formControlName=\"password\">\n        </label>\n\n        <label>Email\n          <input type=\"email\" formControlName=\"email\" required email=\"true\">\n        </label>\n        <div *ngIf=\"officialEmail.errors\"> \n          <div *ngIf=\"officialEmail.errors.required\">\n              Correct Email required.\n          </div> \t\t   \n          <div *ngIf=\"officialEmail.errors.pattern\">\n              Email not valid.\n          </div> \n       </div> \n        <!-- <label>Email\n          <input type=\"email\" formControlName=\"email\" required>\n        </label> -->\n        <!-- <div class=\"alert\" *ngIf=\"!rForm.controls['email'].valid && rForm.controls['email'].touched\">Give valid mailid</div> -->\n        <!-- <div *ngIf=\"primaryEmail.errors?.email\">\n          Primary Email not valid.\n     </div>  -->\n\n        <!-- <label>Password\n          <textarea  formControlName=\"description\"></textarea>\n        </label> -->\n        <!-- <div class=\"alert\" *ngIf=\"!rForm.controls['description'].valid && rForm.controls['description'].touched\">You must specify a description that's between 30 and 500 characters.</div> -->\n\n        <!-- <label for=\"validate\">Minimum of 3 Characters</label> -->\n        <!-- <input type=\"checkbox\" name=\"validate\" formControlName=\"validate\" value=\"1\"> On -->\n        \n        <input type=\"submit\" class=\"button expanded\" value=\"Submit Form\" [disabled]=\"!rForm.valid\">\n      </div>\n    </div>\n  </form>\n</div>\n\n<!-- <ng-template #forminfo>\n  <div class=\"form-container\">\n    <div class=\"row columns\">\n        <h1>{{ name }} has registered</h1>\n\n        <p>{{ description }}</p> \n    </div>\n  </div>\n</ng-template> -->"

/***/ }),

/***/ "./src/app/home-module/component/registration/registration.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/home-module/component/registration/registration.component.ts ***!
  \******************************************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_registration_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/registration.service */ "./src/app/services/registration.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegistrationComponent = /** @class */ (function () {
    function RegistrationComponent(fb, service, router) {
        this.fb = fb;
        this.service = service;
        this.router = router;
        this.username = ''; //password
        this.password = '';
        this.role = 'user';
        this.titleAlert = 'This field is required';
        this.rForm = fb.group({
            'username': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'validate': '',
            'email': ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.emailPattern)]],
            'role': 'user',
        });
    }
    RegistrationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.rForm.get('validate').valueChanges.subscribe(function (validate) {
            if (validate == '1') {
                _this.rForm.get('username').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3)]);
                _this.titleAlert = 'You need to specify at least 3 characters';
            }
            else {
                _this.rForm.get('username').setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required);
            }
            _this.rForm.get('username').updateValueAndValidity();
        });
    };
    RegistrationComponent.prototype.addPost = function (post) {
        var _this = this;
        this.service.addPost(post).subscribe(function (res) {
            console.log("You are registered!::", res);
            console.log('res====', res);
            localStorage.setItem('LoggedInUser', 'true');
            localStorage.setItem('id', res.data._id);
            localStorage.setItem('token', res.data.token);
            localStorage.setItem("role", res.data.role);
            _this.router.navigate(['/userdashboard']);
        });
    };
    Object.defineProperty(RegistrationComponent.prototype, "officialEmail", {
        get: function () {
            return this.rForm.get('email');
        },
        enumerable: true,
        configurable: true
    });
    RegistrationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-registration',
            template: __webpack_require__(/*! ./registration.component.html */ "./src/app/home-module/component/registration/registration.component.html"),
            styles: [__webpack_require__(/*! ./registration.component.css */ "./src/app/home-module/component/registration/registration.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _services_registration_service__WEBPACK_IMPORTED_MODULE_2__["RegistrationService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], RegistrationComponent);
    return RegistrationComponent;
}());



/***/ }),

/***/ "./src/app/home-module/home-module-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/home-module/home-module-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: HomeModuleRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModuleRoutingModule", function() { return HomeModuleRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


// import { HomepageComponent } from './component/homepage/homepage.component';
var routes = [
// {
//   path:'',
//   component:HomepageComponent
// }
];
var HomeModuleRoutingModule = /** @class */ (function () {
    function HomeModuleRoutingModule() {
    }
    HomeModuleRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], HomeModuleRoutingModule);
    return HomeModuleRoutingModule;
}());



/***/ }),

/***/ "./src/app/home-module/home-module.module.ts":
/*!***************************************************!*\
  !*** ./src/app/home-module/home-module.module.ts ***!
  \***************************************************/
/*! exports provided: HomeModuleModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModuleModule", function() { return HomeModuleModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_module_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-module-routing.module */ "./src/app/home-module/home-module-routing.module.ts");
/* harmony import */ var _component_blogs_blogs_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./component/blogs/blogs.component */ "./src/app/home-module/component/blogs/blogs.component.ts");
/* harmony import */ var _component_homepage_homepage_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./component/homepage/homepage.component */ "./src/app/home-module/component/homepage/homepage.component.ts");
/* harmony import */ var _component_registration_registration_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./component/registration/registration.component */ "./src/app/home-module/component/registration/registration.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_login_login_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./component/login/login.component */ "./src/app/home-module/component/login/login.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/card */ "./node_modules/primeng/card.js");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(primeng_card__WEBPACK_IMPORTED_MODULE_11__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var HomeModuleModule = /** @class */ (function () {
    function HomeModuleModule() {
    }
    HomeModuleModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _home_module_routing_module__WEBPACK_IMPORTED_MODULE_3__["HomeModuleRoutingModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__["BrowserAnimationsModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_10__["ToastrModule"].forRoot(),
                primeng_card__WEBPACK_IMPORTED_MODULE_11__["CardModule"],
            ],
            declarations: [_component_blogs_blogs_component__WEBPACK_IMPORTED_MODULE_4__["BlogsComponent"], _component_homepage_homepage_component__WEBPACK_IMPORTED_MODULE_5__["HomepageComponent"], _component_registration_registration_component__WEBPACK_IMPORTED_MODULE_6__["RegistrationComponent"], _component_login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"]]
        })
    ], HomeModuleModule);
    return HomeModuleModule;
}());



/***/ }),

/***/ "./src/app/services/auth-services.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/auth-services.service.ts ***!
  \***************************************************/
/*! exports provided: AuthServicesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthServicesService", function() { return AuthServicesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { Observable } from 'rxjs/Observable';
var AuthServicesService = /** @class */ (function () {
    function AuthServicesService(http) {
        this.http = http;
    }
    AuthServicesService.prototype.getToken = function () {
        return localStorage.getItem("token");
    };
    AuthServicesService.prototype.isLoggednIn = function () {
        return this.getToken() !== null;
    };
    AuthServicesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthServicesService);
    return AuthServicesService;
}());



/***/ }),

/***/ "./src/app/services/login.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/login.service.ts ***!
  \*******************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoginService = /** @class */ (function () {
    function LoginService(http) {
        this.http = http;
    }
    LoginService.prototype.login = function (data) {
        return this.http.post('http://localhost:10010/login', data);
    };
    LoginService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/services/registration.service.ts":
/*!**************************************************!*\
  !*** ./src/app/services/registration.service.ts ***!
  \**************************************************/
/*! exports provided: RegistrationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationService", function() { return RegistrationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RegistrationService = /** @class */ (function () {
    function RegistrationService(http) {
        this.http = http;
    }
    RegistrationService.prototype.addPost = function (data) {
        return this.http.post('http://localhost:10010/registration', data);
    };
    RegistrationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], RegistrationService);
    return RegistrationService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/nupurjaurkar/Documents/First Component Project/frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map