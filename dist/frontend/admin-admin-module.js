(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["admin-admin-module"],{

/***/ "./node_modules/primeng/button.js":
/*!****************************************!*\
  !*** ./node_modules/primeng/button.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* Shorthand */

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(/*! ./components/button/button */ "./node_modules/primeng/components/button/button.js"));

/***/ }),

/***/ "./node_modules/primeng/components/button/button.js":
/*!**********************************************************!*\
  !*** ./node_modules/primeng/components/button/button.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var domhandler_1 = __webpack_require__(/*! ../dom/domhandler */ "./node_modules/primeng/components/dom/domhandler.js");
var common_1 = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var ButtonDirective = /** @class */ (function () {
    function ButtonDirective(el, domHandler) {
        this.el = el;
        this.domHandler = domHandler;
        this.iconPos = 'left';
        this.cornerStyleClass = 'ui-corner-all';
    }
    ButtonDirective.prototype.ngAfterViewInit = function () {
        this.domHandler.addMultipleClasses(this.el.nativeElement, this.getStyleClass());
        if (this.icon) {
            var iconElement = document.createElement("span");
            iconElement.setAttribute("aria-hidden", "true");
            var iconPosClass = (this.iconPos == 'right') ? 'ui-button-icon-right' : 'ui-button-icon-left';
            iconElement.className = iconPosClass + ' ui-clickable ' + this.icon;
            this.el.nativeElement.appendChild(iconElement);
        }
        var labelElement = document.createElement("span");
        labelElement.className = 'ui-button-text ui-clickable';
        labelElement.appendChild(document.createTextNode(this.label || 'ui-btn'));
        this.el.nativeElement.appendChild(labelElement);
        this.initialized = true;
    };
    ButtonDirective.prototype.getStyleClass = function () {
        var styleClass = 'ui-button ui-widget ui-state-default ' + this.cornerStyleClass;
        if (this.icon) {
            if (this.label != null && this.label != undefined) {
                if (this.iconPos == 'left')
                    styleClass = styleClass + ' ui-button-text-icon-left';
                else
                    styleClass = styleClass + ' ui-button-text-icon-right';
            }
            else {
                styleClass = styleClass + ' ui-button-icon-only';
            }
        }
        else {
            if (this.label) {
                styleClass = styleClass + ' ui-button-text-only';
            }
            else {
                styleClass = styleClass + ' ui-button-text-empty';
            }
        }
        return styleClass;
    };
    Object.defineProperty(ButtonDirective.prototype, "label", {
        get: function () {
            return this._label;
        },
        set: function (val) {
            this._label = val;
            if (this.initialized) {
                this.domHandler.findSingle(this.el.nativeElement, '.ui-button-text').textContent = this._label;
                if (!this.icon) {
                    if (this._label) {
                        this.domHandler.removeClass(this.el.nativeElement, 'ui-button-text-empty');
                        this.domHandler.addClass(this.el.nativeElement, 'ui-button-text-only');
                    }
                    else {
                        this.domHandler.addClass(this.el.nativeElement, 'ui-button-text-empty');
                        this.domHandler.removeClass(this.el.nativeElement, 'ui-button-text-only');
                    }
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ButtonDirective.prototype, "icon", {
        get: function () {
            return this._icon;
        },
        set: function (val) {
            this._icon = val;
            if (this.initialized) {
                var iconPosClass = (this.iconPos == 'right') ? 'ui-button-icon-right' : 'ui-button-icon-left';
                this.domHandler.findSingle(this.el.nativeElement, '.ui-clickable').className =
                    iconPosClass + ' ui-clickable ' + this.icon;
            }
        },
        enumerable: true,
        configurable: true
    });
    ButtonDirective.prototype.ngOnDestroy = function () {
        while (this.el.nativeElement.hasChildNodes()) {
            this.el.nativeElement.removeChild(this.el.nativeElement.lastChild);
        }
        this.initialized = false;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ButtonDirective.prototype, "iconPos", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ButtonDirective.prototype, "cornerStyleClass", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], ButtonDirective.prototype, "label", null);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], ButtonDirective.prototype, "icon", null);
    ButtonDirective = __decorate([
        core_1.Directive({
            selector: '[pButton]',
            providers: [domhandler_1.DomHandler]
        }),
        __metadata("design:paramtypes", [core_1.ElementRef, domhandler_1.DomHandler])
    ], ButtonDirective);
    return ButtonDirective;
}());
exports.ButtonDirective = ButtonDirective;
var Button = /** @class */ (function () {
    function Button() {
        this.iconPos = 'left';
        this.onClick = new core_1.EventEmitter();
        this.onFocus = new core_1.EventEmitter();
        this.onBlur = new core_1.EventEmitter();
    }
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Button.prototype, "type", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Button.prototype, "iconPos", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Button.prototype, "icon", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Button.prototype, "label", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Button.prototype, "disabled", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], Button.prototype, "style", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Button.prototype, "styleClass", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Button.prototype, "onClick", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Button.prototype, "onFocus", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Button.prototype, "onBlur", void 0);
    Button = __decorate([
        core_1.Component({
            selector: 'p-button',
            template: "\n        <button [attr.type]=\"type\" [class]=\"styleClass\" [style]=\"style\" [disabled]=\"disabled\"\n            [ngClass]=\"{'ui-button ui-widget ui-state-default ui-corner-all':true,\n                        'ui-button-icon-only': (icon && !label),\n                        'ui-button-text-icon-left': (icon && label && iconPos === 'left'),\n                        'ui-button-text-icon-right': (icon && label && iconPos === 'right'),\n                        'ui-button-text-only': (!icon && label),\n                        'ui-button-text-empty': (!icon && !label),\n                        'ui-state-disabled': disabled}\"\n                        (click)=\"onClick.emit($event)\" (focus)=\"onFocus.emit($event)\" (blur)=\"onBlur.emit($event)\">\n            <ng-content></ng-content>\n            <span [ngClass]=\"{'ui-clickable': true,\n                        'ui-button-icon-left': (iconPos === 'left'), \n                        'ui-button-icon-right': (iconPos === 'right')}\"\n                        [class]=\"icon\" *ngIf=\"icon\"></span>\n            <span class=\"ui-button-text ui-clickable\">{{label||'ui-btn'}}</span>\n        </button>\n    "
        })
    ], Button);
    return Button;
}());
exports.Button = Button;
var ButtonModule = /** @class */ (function () {
    function ButtonModule() {
    }
    ButtonModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule],
            exports: [ButtonDirective, Button],
            declarations: [ButtonDirective, Button]
        })
    ], ButtonModule);
    return ButtonModule;
}());
exports.ButtonModule = ButtonModule;
//# sourceMappingURL=button.js.map

/***/ }),

/***/ "./node_modules/primeng/components/editor/editor.js":
/*!**********************************************************!*\
  !*** ./node_modules/primeng/components/editor/editor.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var common_1 = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var shared_1 = __webpack_require__(/*! ../common/shared */ "./node_modules/primeng/components/common/shared.js");
var domhandler_1 = __webpack_require__(/*! ../dom/domhandler */ "./node_modules/primeng/components/dom/domhandler.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
exports.EDITOR_VALUE_ACCESSOR = {
    provide: forms_1.NG_VALUE_ACCESSOR,
    useExisting: core_1.forwardRef(function () { return Editor; }),
    multi: true
};
var Editor = /** @class */ (function () {
    function Editor(el, domHandler) {
        this.el = el;
        this.domHandler = domHandler;
        this.onTextChange = new core_1.EventEmitter();
        this.onSelectionChange = new core_1.EventEmitter();
        this.onInit = new core_1.EventEmitter();
        this.onModelChange = function () { };
        this.onModelTouched = function () { };
    }
    Editor.prototype.ngAfterViewInit = function () {
        var _this = this;
        var editorElement = this.domHandler.findSingle(this.el.nativeElement, 'div.ui-editor-content');
        var toolbarElement = this.domHandler.findSingle(this.el.nativeElement, 'div.ui-editor-toolbar');
        this.quill = new Quill(editorElement, {
            modules: {
                toolbar: toolbarElement
            },
            placeholder: this.placeholder,
            readOnly: this.readonly,
            theme: 'snow',
            formats: this.formats
        });
        if (this.value) {
            this.quill.pasteHTML(this.value);
        }
        this.quill.on('text-change', function (delta, oldContents, source) {
            if (source === 'user') {
                var html = editorElement.children[0].innerHTML;
                var text = _this.quill.getText().trim();
                if (text.length === 0) {
                    html = null;
                }
                _this.onTextChange.emit({
                    htmlValue: html,
                    textValue: text,
                    delta: delta,
                    source: source
                });
                _this.onModelChange(html);
                _this.onModelTouched();
            }
        });
        this.quill.on('selection-change', function (range, oldRange, source) {
            _this.onSelectionChange.emit({
                range: range,
                oldRange: oldRange,
                source: source
            });
        });
        this.onInit.emit({
            editor: this.quill
        });
    };
    Editor.prototype.writeValue = function (value) {
        this.value = value;
        if (this.quill) {
            if (value)
                this.quill.pasteHTML(value);
            else
                this.quill.setText('');
        }
    };
    Editor.prototype.registerOnChange = function (fn) {
        this.onModelChange = fn;
    };
    Editor.prototype.registerOnTouched = function (fn) {
        this.onModelTouched = fn;
    };
    Editor.prototype.getQuill = function () {
        return this.quill;
    };
    Object.defineProperty(Editor.prototype, "readonly", {
        get: function () {
            return this._readonly;
        },
        set: function (val) {
            this._readonly = val;
            if (this.quill) {
                if (this._readonly)
                    this.quill.disable();
                else
                    this.quill.enable();
            }
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Editor.prototype, "onTextChange", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Editor.prototype, "onSelectionChange", void 0);
    __decorate([
        core_1.ContentChild(shared_1.Header),
        __metadata("design:type", Object)
    ], Editor.prototype, "toolbar", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], Editor.prototype, "style", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Editor.prototype, "styleClass", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Editor.prototype, "placeholder", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], Editor.prototype, "formats", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Editor.prototype, "onInit", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], Editor.prototype, "readonly", null);
    Editor = __decorate([
        core_1.Component({
            selector: 'p-editor',
            template: "\n        <div [ngClass]=\"'ui-widget ui-editor-container ui-corner-all'\" [class]=\"styleClass\">\n            <div class=\"ui-editor-toolbar ui-widget-header ui-corner-top\" *ngIf=\"toolbar\">\n                <ng-content select=\"p-header\"></ng-content>\n            </div>\n            <div class=\"ui-editor-toolbar ui-widget-header ui-corner-top\" *ngIf=\"!toolbar\">\n                <span class=\"ql-formats\">\n                    <select class=\"ql-header\">\n                      <option value=\"1\">Heading</option>\n                      <option value=\"2\">Subheading</option>\n                      <option selected>Normal</option>\n                    </select>\n                    <select class=\"ql-font\">\n                      <option selected>Sans Serif</option>\n                      <option value=\"serif\">Serif</option>\n                      <option value=\"monospace\">Monospace</option>\n                    </select>\n                </span>\n                <span class=\"ql-formats\">\n                    <button class=\"ql-bold\" aria-label=\"Bold\"></button>\n                    <button class=\"ql-italic\" aria-label=\"Italic\"></button>\n                    <button class=\"ql-underline\" aria-label=\"Underline\"></button>\n                </span>\n                <span class=\"ql-formats\">\n                    <select class=\"ql-color\"></select>\n                    <select class=\"ql-background\"></select>\n                </span>\n                <span class=\"ql-formats\">\n                    <button class=\"ql-list\" value=\"ordered\" aria-label=\"Ordered List\"></button>\n                    <button class=\"ql-list\" value=\"bullet\" aria-label=\"Unordered List\"></button>\n                    <select class=\"ql-align\">\n                        <option selected></option>\n                        <option value=\"center\"></option>\n                        <option value=\"right\"></option>\n                        <option value=\"justify\"></option>\n                    </select>\n                </span>\n                <span class=\"ql-formats\">\n                    <button class=\"ql-link\" aria-label=\"Insert Link\"></button>\n                    <button class=\"ql-image\" aria-label=\"Insert Image\"></button>\n                    <button class=\"ql-code-block\" aria-label=\"Insert Code Block\"></button>\n                </span>\n                <span class=\"ql-formats\">\n                    <button class=\"ql-clean\" aria-label=\"Remove Styles\"></button>\n                </span>\n            </div>\n            <div class=\"ui-editor-content\" [ngStyle]=\"style\"></div>\n        </div>\n    ",
            providers: [domhandler_1.DomHandler, exports.EDITOR_VALUE_ACCESSOR]
        }),
        __metadata("design:paramtypes", [core_1.ElementRef, domhandler_1.DomHandler])
    ], Editor);
    return Editor;
}());
exports.Editor = Editor;
var EditorModule = /** @class */ (function () {
    function EditorModule() {
    }
    EditorModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule],
            exports: [Editor, shared_1.SharedModule],
            declarations: [Editor]
        })
    ], EditorModule);
    return EditorModule;
}());
exports.EditorModule = EditorModule;
//# sourceMappingURL=editor.js.map

/***/ }),

/***/ "./node_modules/primeng/editor.js":
/*!****************************************!*\
  !*** ./node_modules/primeng/editor.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* Shorthand */

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(/*! ./components/editor/editor */ "./node_modules/primeng/components/editor/editor.js"));

/***/ }),

/***/ "./src/app/admin/admin-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/admin/admin-routing.module.ts ***!
  \***********************************************/
/*! exports provided: AdminRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminRoutingModule", function() { return AdminRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./src/app/admin/components/dashboard/dashboard.component.ts");
/* harmony import */ var _components_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/contact-us/contact-us.component */ "./src/app/admin/components/contact-us/contact-us.component.ts");
/* harmony import */ var _components_reg_user_blogs_reg_user_blogs_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/reg-user-blogs/reg-user-blogs.component */ "./src/app/admin/components/reg-user-blogs/reg-user-blogs.component.ts");
/* harmony import */ var _components_reg_user_blogs_components_write_blogs_write_blogs_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/reg-user-blogs/components/write-blogs/write-blogs.component */ "./src/app/admin/components/reg-user-blogs/components/write-blogs/write-blogs.component.ts");
/* harmony import */ var _components_reg_user_blogs_components_blog_listing_blog_listing_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/reg-user-blogs/components/blog-listing/blog-listing.component */ "./src/app/admin/components/reg-user-blogs/components/blog-listing/blog-listing.component.ts");
/* harmony import */ var _components_reg_user_blogs_components_edit_blogs_edit_blogs_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/reg-user-blogs/components/edit-blogs/edit-blogs.component */ "./src/app/admin/components/reg-user-blogs/components/edit-blogs/edit-blogs.component.ts");
/* harmony import */ var _admin_components_manage_users_manage_users_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../admin/components/manage-users/manage-users.component */ "./src/app/admin/components/manage-users/manage-users.component.ts");
/* harmony import */ var _components_manage_users_components_add_user_add_user_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/manage-users/components/add-user/add-user.component */ "./src/app/admin/components/manage-users/components/add-user/add-user.component.ts");
/* harmony import */ var _components_manage_users_components_edit_users_edit_users_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/manage-users/components/edit-users/edit-users.component */ "./src/app/admin/components/manage-users/components/edit-users/edit-users.component.ts");
/* harmony import */ var _components_manage_products_manage_products_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/manage-products/manage-products.component */ "./src/app/admin/components/manage-products/manage-products.component.ts");
/* harmony import */ var _components_manage_products_components_edit_products_edit_products_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/manage-products/components/edit-products/edit-products.component */ "./src/app/admin/components/manage-products/components/edit-products/edit-products.component.ts");
/* harmony import */ var _components_manage_products_components_add_products_add_products_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/manage-products/components/add-products/add-products.component */ "./src/app/admin/components/manage-products/components/add-products/add-products.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var routes = [
    {
        path: '',
        component: _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"]
    },
    {
        path: 'contact-us',
        component: _components_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_4__["ContactUsComponent"]
    },
    {
        path: 'reg-user-blogs',
        component: _components_reg_user_blogs_reg_user_blogs_component__WEBPACK_IMPORTED_MODULE_5__["RegUserBlogsComponent"]
    },
    {
        path: 'write-blogs',
        component: _components_reg_user_blogs_components_write_blogs_write_blogs_component__WEBPACK_IMPORTED_MODULE_6__["WriteBlogsComponent"]
    },
    {
        path: 'blogs-listing',
        component: _components_reg_user_blogs_components_blog_listing_blog_listing_component__WEBPACK_IMPORTED_MODULE_7__["BlogListingComponent"]
    },
    {
        path: 'edit-blogs/:id',
        component: _components_reg_user_blogs_components_edit_blogs_edit_blogs_component__WEBPACK_IMPORTED_MODULE_8__["EditBlogsComponent"]
    },
    {
        path: 'manage-users',
        component: _admin_components_manage_users_manage_users_component__WEBPACK_IMPORTED_MODULE_9__["ManageUsersComponent"]
    },
    {
        path: 'add-users',
        component: _components_manage_users_components_add_user_add_user_component__WEBPACK_IMPORTED_MODULE_10__["AddUserComponent"]
    },
    {
        path: 'edit-users/:id',
        component: _components_manage_users_components_edit_users_edit_users_component__WEBPACK_IMPORTED_MODULE_11__["EditUsersComponent"]
    },
    {
        path: 'manage-products',
        component: _components_manage_products_manage_products_component__WEBPACK_IMPORTED_MODULE_12__["ManageProductsComponent"]
    },
    {
        path: 'edit-products/:id',
        component: _components_manage_products_components_edit_products_edit_products_component__WEBPACK_IMPORTED_MODULE_13__["EditProductsComponent"]
    },
    {
        path: 'add-products',
        component: _components_manage_products_components_add_products_add_products_component__WEBPACK_IMPORTED_MODULE_14__["AddProductsComponent"]
    },
];
var AdminRoutingModule = /** @class */ (function () {
    function AdminRoutingModule() {
    }
    AdminRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AdminRoutingModule);
    return AdminRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/admin.module.ts":
/*!***************************************!*\
  !*** ./src/app/admin/admin.module.ts ***!
  \***************************************/
/*! exports provided: AdminModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminModule", function() { return AdminModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./src/app/admin/components/dashboard/dashboard.component.ts");
/* harmony import */ var _components_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/contact-us/contact-us.component */ "./src/app/admin/components/contact-us/contact-us.component.ts");
/* harmony import */ var _components_reg_user_blogs_reg_user_blogs_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/reg-user-blogs/reg-user-blogs.component */ "./src/app/admin/components/reg-user-blogs/reg-user-blogs.component.ts");
/* harmony import */ var _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/navbar/navbar.component */ "./src/app/admin/components/navbar/navbar.component.ts");
/* harmony import */ var _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/footer/footer.component */ "./src/app/admin/components/footer/footer.component.ts");
/* harmony import */ var _components_reg_user_blogs_components_write_blogs_write_blogs_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/reg-user-blogs/components/write-blogs/write-blogs.component */ "./src/app/admin/components/reg-user-blogs/components/write-blogs/write-blogs.component.ts");
/* harmony import */ var _components_reg_user_blogs_components_blog_listing_blog_listing_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/reg-user-blogs/components/blog-listing/blog-listing.component */ "./src/app/admin/components/reg-user-blogs/components/blog-listing/blog-listing.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _components_reg_user_blogs_components_edit_blogs_edit_blogs_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/reg-user-blogs/components/edit-blogs/edit-blogs.component */ "./src/app/admin/components/reg-user-blogs/components/edit-blogs/edit-blogs.component.ts");
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ng2-search-filter */ "./node_modules/ng2-search-filter/ng2-search-filter.es5.js");
/* harmony import */ var _admin_routing_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./admin-routing.module */ "./src/app/admin/admin-routing.module.ts");
/* harmony import */ var _components_manage_users_manage_users_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/manage-users/manage-users.component */ "./src/app/admin/components/manage-users/manage-users.component.ts");
/* harmony import */ var _components_manage_users_components_add_user_add_user_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/manage-users/components/add-user/add-user.component */ "./src/app/admin/components/manage-users/components/add-user/add-user.component.ts");
/* harmony import */ var _components_manage_users_components_edit_users_edit_users_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/manage-users/components/edit-users/edit-users.component */ "./src/app/admin/components/manage-users/components/edit-users/edit-users.component.ts");
/* harmony import */ var _components_manage_products_manage_products_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/manage-products/manage-products.component */ "./src/app/admin/components/manage-products/manage-products.component.ts");
/* harmony import */ var _components_manage_products_components_add_products_add_products_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/manage-products/components/add-products/add-products.component */ "./src/app/admin/components/manage-products/components/add-products/add-products.component.ts");
/* harmony import */ var _components_manage_products_components_edit_products_edit_products_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/manage-products/components/edit-products/edit-products.component */ "./src/app/admin/components/manage-products/components/edit-products/edit-products.component.ts");
/* harmony import */ var primeng_editor__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! primeng/editor */ "./node_modules/primeng/editor.js");
/* harmony import */ var primeng_editor__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(primeng_editor__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! primeng/table */ "./node_modules/primeng/table.js");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(primeng_table__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! primeng/button */ "./node_modules/primeng/button.js");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(primeng_button__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var primeng_paginator__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! primeng/paginator */ "./node_modules/primeng/paginator.js");
/* harmony import */ var primeng_paginator__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(primeng_paginator__WEBPACK_IMPORTED_MODULE_22__);
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! primeng/card */ "./node_modules/primeng/card.js");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(primeng_card__WEBPACK_IMPORTED_MODULE_23__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















// import { FileUploader,FileSelectDirective } from 'ng2-file-upload';





var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _admin_routing_module__WEBPACK_IMPORTED_MODULE_12__["AdminRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                ng2_search_filter__WEBPACK_IMPORTED_MODULE_11__["Ng2SearchPipeModule"],
                primeng_editor__WEBPACK_IMPORTED_MODULE_19__["EditorModule"],
                primeng_table__WEBPACK_IMPORTED_MODULE_20__["TableModule"],
                primeng_button__WEBPACK_IMPORTED_MODULE_21__["ButtonModule"],
                primeng_paginator__WEBPACK_IMPORTED_MODULE_22__["PaginatorModule"],
                primeng_card__WEBPACK_IMPORTED_MODULE_23__["CardModule"],
            ],
            declarations: [_components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_2__["DashboardComponent"],
                _components_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_3__["ContactUsComponent"],
                _components_reg_user_blogs_reg_user_blogs_component__WEBPACK_IMPORTED_MODULE_4__["RegUserBlogsComponent"],
                _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_5__["NavbarComponent"],
                _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_6__["FooterComponent"],
                _components_reg_user_blogs_components_write_blogs_write_blogs_component__WEBPACK_IMPORTED_MODULE_7__["WriteBlogsComponent"],
                _components_reg_user_blogs_components_blog_listing_blog_listing_component__WEBPACK_IMPORTED_MODULE_8__["BlogListingComponent"],
                _components_reg_user_blogs_components_edit_blogs_edit_blogs_component__WEBPACK_IMPORTED_MODULE_10__["EditBlogsComponent"],
                _components_manage_users_manage_users_component__WEBPACK_IMPORTED_MODULE_13__["ManageUsersComponent"],
                _components_manage_users_components_add_user_add_user_component__WEBPACK_IMPORTED_MODULE_14__["AddUserComponent"],
                _components_manage_users_components_edit_users_edit_users_component__WEBPACK_IMPORTED_MODULE_15__["EditUsersComponent"],
                _components_manage_products_manage_products_component__WEBPACK_IMPORTED_MODULE_16__["ManageProductsComponent"],
                _components_manage_products_components_add_products_add_products_component__WEBPACK_IMPORTED_MODULE_17__["AddProductsComponent"],
                _components_manage_products_components_edit_products_edit_products_component__WEBPACK_IMPORTED_MODULE_18__["EditProductsComponent"],]
        })
    ], AdminModule);
    return AdminModule;
}());



/***/ }),

/***/ "./src/app/admin/components/contact-us/contact-us.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/admin/components/contact-us/contact-us.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/components/contact-us/contact-us.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/admin/components/contact-us/contact-us.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <!DOCTYPE html>\n<html>\n<head>\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n<style>\nbody {\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n* {\n    box-sizing: border-box;\n}\n\n/* Style inputs */\ninput[type=text], select, textarea {\n    width: 100%;\n    padding: 12px;\n    border: 1px solid #ccc;\n    margin-top: 6px;\n    margin-bottom: 16px;\n    resize: vertical;\n}\n\ninput[type=submit] {\n    background-color: #4CAF50;\n    color: white;\n    padding: 12px 20px;\n    border: none;\n    cursor: pointer;\n}\n\ninput[type=submit]:hover {\n    background-color: #45a049;\n}\n\n/* Style the container/contact section */\n.container {\n    border-radius: 5px;\n    background-color: #f2f2f2;\n    padding: 10px;\n}\n\n/* Create two columns that float next to eachother */\n.column {\n    float: left;\n    width: 50%;\n    margin-top: 6px;\n    padding: 100px 20px;\n}\n\n/* Clear floats after the columns */\n.row:after {\n    content: \"\";\n    display: table;\n    clear: both;\n}\n\n/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */\n@media screen and (max-width: 600px) {\n    .column, input[type=submit] {\n        width: 100%;\n        margin-top: 0;\n    }\n}\n</style>\n</head>\n<body>\n    <app-navbar></app-navbar>\n<div class=\"container\">\n  <div style=\"text-align:center\">\n    <h2>Contact Us</h2>\n    <p>Swing by for a cup of coffee, or leave us a message:</p>\n  </div>\n  <div class=\"row\">\n    <div class=\"column\">\n      <div id=\"map\" style=\"width:100%;height:500px\"></div>\n    </div>\n    <div class=\"column\">\n      <form action=\"/action_page.php\">\n        <label for=\"fname\">First Name</label>\n        <input type=\"text\" id=\"fname\" name=\"firstname\" placeholder=\"Your name..\">\n        <label for=\"lname\">Last Name</label>\n        <input type=\"text\" id=\"lname\" name=\"lastname\" placeholder=\"Your last name..\">\n        <label for=\"country\">Country</label>\n        <select id=\"country\" name=\"country\">\n          <option value=\"australia\">Australia</option>\n          <option value=\"canada\">Canada</option>\n          <option value=\"usa\">USA</option>\n        </select>\n        <label for=\"subject\">Subject</label>\n        <textarea id=\"subject\" name=\"subject\" placeholder=\"Write something..\" style=\"height:170px\"></textarea>\n        <input type=\"submit\" value=\"Submit\">\n      </form>\n    </div>\n  </div>\n</div>\n\n<script>\n// Initialize google maps\nfunction myMap() {\n  var myCenter = new google.maps.LatLng(51.508742,-0.120850);\n  var mapCanvas = document.getElementById(\"map\");\n  var mapOptions = {center: myCenter, zoom: 12};\n  var map = new google.maps.Map(mapCanvas, mapOptions);\n  var marker = new google.maps.Marker({position:myCenter});\n  marker.setMap(map);\n}\n</script>\n\n<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap\"></script>\n<!--\nTo use this code on your website, get a free API key from Google.\nRead more at: https://www.w3schools.com/graphics/google_maps_basic.asp\n-->\n\n<!-- </body> -->\n\n<!-- </html>  -->\n<!DOCTYPE html>\n<html>\n<head>\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">\n<style>\n.card {\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n  max-width: 300px;\n  margin: auto;\n  text-align: center;\n  font-family: arial;\n}\n\n.title {\n  color: grey;\n  font-size: 18px;\n}\n\nbutton {\n  border: none;\n  outline: 0;\n  display: inline-block;\n  padding: 8px;\n  color: white;\n  background-color: #000;\n  text-align: center;\n  cursor: pointer;\n  width: 100%;\n  font-size: 18px;\n}\n\na {\n  text-decoration: none;\n  font-size: 22px;\n  color: black;\n}\n\nbutton:hover, a:hover {\n  opacity: 0.7;\n}\n</style>\n</head>\n<app-navbar></app-navbar>\n<body>\n\n<h2 style=\"text-align:center\">Ecommerce Card</h2>\n\n<div class=\"card\">\n  <!-- <img src=\"/w3images/team2.jpg\" alt=\"John\" style=\"width:100%\"> -->\n  <h1>Ecommerce</h1>\n  <p class=\"title\">Your own blogging and shopping site</p>\n  <p>Creation at its best</p>\n  <div style=\"margin: 24px 0;\">\n    <a href=\"#\"><i class=\"fa fa-dribbble\"></i></a> \n    <a href=\"#\"><i class=\"fa fa-twitter\"></i></a>  \n    <a href=\"#\"><i class=\"fa fa-linkedin\"></i></a>  \n    <a href=\"#\"><i class=\"fa fa-facebook\"></i></a> \n </div>\n <p>Contact</p>\n <p>SunTec Web Services Pvt. Ltd.  <br>\n    Floor 3, Vardhman Times Plaza<br>\n    Plot 13, DDA Community Centre<br>\n    Road 44, Pitampura <br>\n    New Delhi - 110 034, INDIA<br> \n    Phone:    91-11-42644425 <br>\n                     91-11-42644426<br> \n                     91-11-42644427 <br>\n                     91-11-42644428 <br>\n                     91-11-42644429 <br>\n    Fax (India): 91-11-42644430 </p>\n</div>\n\n</body>\n<app-footer></app-footer>\n\n</html>\n"

/***/ }),

/***/ "./src/app/admin/components/contact-us/contact-us.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/admin/components/contact-us/contact-us.component.ts ***!
  \*********************************************************************/
/*! exports provided: ContactUsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsComponent", function() { return ContactUsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ContactUsComponent = /** @class */ (function () {
    function ContactUsComponent() {
    }
    ContactUsComponent.prototype.ngOnInit = function () {
    };
    ContactUsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contact-us',
            template: __webpack_require__(/*! ./contact-us.component.html */ "./src/app/admin/components/contact-us/contact-us.component.html"),
            styles: [__webpack_require__(/*! ./contact-us.component.css */ "./src/app/admin/components/contact-us/contact-us.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ContactUsComponent);
    return ContactUsComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/dashboard/dashboard.component.css":
/*!********************************************************************!*\
  !*** ./src/app/admin/components/dashboard/dashboard.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".panel-body img{\n    height: 200px;\n    width: auto\n}"

/***/ }),

/***/ "./src/app/admin/components/dashboard/dashboard.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/admin/components/dashboard/dashboard.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <!DOCTYPE html>\n<html lang=\"en\">\n<head>\n  <title>User Welcome</title>\n  <meta charset=\"utf-8\">\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n</head>\n<body>\n\n<nav class=\"navbar navbar-inverse\">\n  <div class=\"container-fluid\">\n    <div class=\"navbar-header\">\n      <a class=\"navbar-brand\" href=\"#\">Home</a>\n    </div>\n    <ul class=\"nav navbar-nav\">\n      <li class=\"active\"><a href=\"#\">Home</a></li>\n      <li><a routerLink ='/profile'>Profile</a></li>\n      <li><a routerLink ='/blog'>Blog</a></li>\n      <li><a routerLink ='/cart'>Cart</a></li>\n      <li><a routerLink ='/logout'>Logout</a></li>\n    </ul>\n  </div>\n</nav>\n</body>\n</html> -->\n\n\n<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n  <title>Welcome User</title>\n  <meta charset=\"utf-8\">\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n  <style>\n    /* Remove the navbar's default rounded borders and increase the bottom margin */ \n    .navbar {\n      margin-bottom: 50px;\n      border-radius: 0;\n    }\n    \n    /* Remove the jumbotron's default bottom margin */ \n     .jumbotron {\n      margin-bottom: 0;\n    }\n   \n    /* Add a gray background color and some padding to the footer */\n    footer {\n      background-color: #f2f2f2;\n      padding: 25px;\n    }\n  </style>\n</head>\n<body>\n<!-- <nav class=\"navbar navbar-inverse\">\n  <div class=\"container-fluid\">\n    <div class=\"navbar-header\">\n      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>                        \n      </button>\n    </div>\n    <div class=\"collapse navbar-collapse\" id=\"myNavbar\">\n      <ul class=\"nav navbar-nav\">\n        <li class=\"active\"><a href=\"#\">Today's deals!</a></li>\n        <li><a routerLink ='/reg-user-blogs/reg-user-blogs'>Blog</a></li>\n        <li><a routerLink ='/contact-us/contact-us'>Contact-us</a></li>\n        <li><a routerLink ='/manage-users/manage-users'>Manage-user</a></li>\n\n      </ul>\n      <ul class=\"nav navbar-nav navbar-right\">\n        <li><a routerLink=\"/account\"><span class=\"glyphicon glyphicon-user\"></span> Your Account</a></li>\n        <li><a routerLink=\"/cart\"><span class=\"glyphicon glyphicon-shopping-cart\"></span> Cart</a></li>\n        <li><a click=\"Logout()\"><span class=\"glyphicon glyphicon-log-out\"></span> Logout</a></li>\n\n      </ul>\n    </div>\n  </div>\n</nav> -->\n<app-navbar></app-navbar>\n\n<div class=\"container\">    \n  <div class=\"row\">\n    <div class=\"col-sm-4\">\n      <div class=\"panel panel-primary\">\n        <div class=\"panel-heading\">BLACK FRIDAY DEAL</div>\n        <div class=\"panel-body\"><img src=\"../../../../assets/images/68526474-1-tf.jpg\" class=\"img-responsive\" style=\"width:100%\" alt=\"Image\"></div>\n        <div class=\"panel-footer\">Buy 50 shirts and get a gift card</div>\n      </div>\n    </div>\n    <div class=\"col-sm-4\"> \n      <div class=\"panel panel-danger\">\n        <div class=\"panel-heading\">BLACK FRIDAY DEAL</div>\n        <div class=\"panel-body\"><img src=\"../../../../assets/images/20077431.jpg\" class=\"img-responsive\" style=\"width:100%\" alt=\"Image\"></div>\n        <div class=\"panel-footer\">Buy 50 shirts and get a gift card</div>\n      </div>\n    </div>\n    <div class=\"col-sm-4\"> \n      <div class=\"panel panel-success\">\n        <div class=\"panel-heading\">BLACK FRIDAY DEAL</div>\n        <div class=\"panel-body\"><img src=\"../../../../assets/images/20082765.jpg\" class=\"img-responsive\" style=\"width:100%\" alt=\"Image\"></div>\n        <div class=\"panel-footer\">Buy 50 shirts and get a gift card</div>\n      </div>\n    </div>\n  </div>\n</div><br>\n\n<div class=\"container\">    \n  <div class=\"row\">\n    <div class=\"col-sm-4\">\n      <div class=\"panel panel-primary\">\n        <div class=\"panel-heading\">BLACK FRIDAY DEAL</div>\n        <div class=\"panel-body\"><img src=\"../../../../assets/images/roymustang.jpg\" class=\"img-responsive\" style=\"width:100%\" alt=\"Image\"></div>\n        <div class=\"panel-footer\">Buy 50 anime posters and get a gift card</div>\n      </div>\n    </div>\n    <div class=\"col-sm-4\"> \n      <div class=\"panel panel-primary\">\n        <div class=\"panel-heading\">BLACK FRIDAY DEAL</div>\n        <div class=\"panel-body\"><img src=\"../../../../assets/images/10295e705b7a5199c1137669a0004704.jpg\" class=\"img-responsive\" alt=\"Image\"></div>\n        <div class=\"panel-footer\">Buy at 50% off!</div>\n      </div>\n    </div>\n    <div class=\"col-sm-4\"> \n      <div class=\"panel panel-primary\">\n        <div class=\"panel-heading\">BLACK FRIDAY DEAL</div>\n        <div class=\"panel-body\"><img src=\"../../../../assets/images/tomoe.jpg\" class=\"img-responsive\" style=\"width:100%\" alt=\"Image\"></div>\n        <div class=\"panel-footer\">Get this print tshirt!</div>\n      </div>\n    </div>\n  </div>\n</div><br><br>\n\n<!-- <footer class=\"container-fluid text-center\">\n  <p>Online Store Copyright</p>  \n  <form class=\"form-inline\">Get deals:\n    <input type=\"email\" class=\"form-control\" size=\"50\" placeholder=\"Email Address\">\n    <button type=\"button\" class=\"btn btn-danger\">Sign Up</button>\n  </form>\n</footer> -->\n<app-footer></app-footer>\n</body>\n</html>\n"

/***/ }),

/***/ "./src/app/admin/components/dashboard/dashboard.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/admin/components/dashboard/dashboard.component.ts ***!
  \*******************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/user.service */ "./src/app/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(router, userService) {
        this.router = router;
        this.userService = userService;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        // this.userService.getUserClaims().subscribe((data: any) => {
        //   this.userClaims = data;
        // });
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/admin/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/admin/components/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/footer/footer.component.css":
/*!**************************************************************!*\
  !*** ./src/app/admin/components/footer/footer.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".footer-bottom {\n    padding: 10px 30px;\n    border-top: 1px solid #666;\n    background: #1e1e1e;\n    position: fixed;\n    left: 0;\n   bottom: 0;\n   width: 100%;\n   /* text-align: center; */\n}\n.copyright-text p {\n    color: #ccc;\n    margin-top: 9px;\n    margin-bottom: 0;\n    text-align: center;\n}\n.social-link li {\n    display: inline-block;\n    margin: 0 5px;\n}\n.social-link li a {\n    color: #ccc;\n    border: 1px solid #ccc;\n    width: 40px;\n    height: 40px;\n    line-height: 40px;\n    border-radius: 50%;\n    text-align: center;\n    display: inline-block;\n    font-size: 14px;\n    transition: .5s;\n}"

/***/ }),

/***/ "./src/app/admin/components/footer/footer.component.html":
/*!***************************************************************!*\
  !*** ./src/app/admin/components/footer/footer.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link href=\"//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">\n<script src=\"//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js\"></script>\n<script src=\"//code.jquery.com/jquery-1.11.1.min.js\"></script>\n<!------ Include the above in your HEAD tag ---------->\n\n<div class=\"footer-bottom\">\n<div class=\"container\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-sm-12 \">\n\t\t\t\t\t\t\t<div class=\"copyright-text\">\n\t\t\t\t\t\t\t\t<p>CopyRight © 2018 Digital All Rights Reserved</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> \n\t\t\t\t\t\t<!-- End Col -->\n\t\t\t\t\t\t<!-- <div class=\"col-sm-6\">\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<ul class=\"social-link pull-right\">\n\t\t\t\t\t\t\t\t<li><a href=\"\"><span class=\"glyphicon glyphicon-heart-empty\"></span></a></li>\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<li><a href=\"\"><span class=\"glyphicon glyphicon-heart-empty\"></span></a></li>\n\t\t\t\t\t\t\t\t<li><a href=\"\"><span class=\"glyphicon glyphicon-heart-empty\"></span></a></li>\n\t\t\t\t\t\t\t\t<li><a href=\"\"><span class=\"glyphicon glyphicon-heart-empty\"></span></a></li>\n\t\t\t\t\t\t\t\t<li><a href=\"\"><span class=\"glyphicon glyphicon-heart-empty\"></span></a></li>\n\t\t\t\t\t\t\t</ul>\t\t\t\t\t\t\t\n\t\t\t\t\t\t</div> End Col -->\n\t\t\t\t\t<!-- </div> -->\n\t\t\t\t</div>\n</div>"

/***/ }),

/***/ "./src/app/admin/components/footer/footer.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/admin/components/footer/footer.component.ts ***!
  \*************************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/admin/components/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/admin/components/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/manage-products/components/add-products/add-products.component.css":
/*!*****************************************************************************************************!*\
  !*** ./src/app/admin/components/manage-products/components/add-products/add-products.component.css ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/components/manage-products/components/add-products/add-products.component.html":
/*!******************************************************************************************************!*\
  !*** ./src/app/admin/components/manage-products/components/add-products/add-products.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.min.css\">\n<div *ngIf=\"!name; else forminfo\">\n  <form [formGroup]=\"rForm\" (ngSubmit)=\"addProducts(rForm.value)\">\n    <div class=\"form-container\">\n      <div class=\"row columns\">\n        <h1>Add Product</h1>\n        <label style=\"font-size: 15px\">Product Name\n          <input type=\"text\" formControlName=\"productName\" class=\"form-control\">\n        </label>\n        <label style=\"font-size: 15px\">Product Description\n          <input type=\"text\" formControlName=\"productDescription\" class=\"form-control\">\n        </label>\n        <label style=\"font-size: 15px\">Product Cost\n          <input type=\"text\" formControlName=\"productCost\" class=\"form-control\">\n        </label>\n        <label style=\"font-size: 15px\">Product Category\n          <input type=\"text\" formControlName=\"productCategory\" class=\"form-control\">\n        </label>\n        <input type=\"file\" id=\"file\" (change)=\"onFileChange($event)\" #fileInput>\n\n        <input type=\"submit\" class=\"button expanded\" value=\"Submit Form\" style=\"font-size: 15px\">\n      </div>\n    </div>\n  </form>\n</div>"

/***/ }),

/***/ "./src/app/admin/components/manage-products/components/add-products/add-products.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/admin/components/manage-products/components/add-products/add-products.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: AddProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddProductsComponent", function() { return AddProductsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddProductsComponent = /** @class */ (function () {
    function AddProductsComponent(fb, service, router, toastr) {
        this.fb = fb;
        this.service = service;
        this.router = router;
        this.toastr = toastr;
        this.productName = '';
        this.productDescription = '';
        this.productCategory = '';
        this.rForm = fb.group({
            'productName': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'productDescription': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'productCost': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'productCategory': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            file: null
            // 'file':[null,Validators.required]
        });
    }
    AddProductsComponent.prototype.ngOnInit = function () {
    };
    AddProductsComponent.prototype.onFileChange = function (event) {
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            this.rForm.get('file').setValue(file);
        }
    };
    AddProductsComponent.prototype.prepareSave = function () {
        var input = new FormData();
        input.append('productName', this.rForm.get('productName').value);
        input.append('productDescription', this.rForm.get('productDescription').value);
        input.append('productCost', this.rForm.get('productCost').value);
        input.append('productCategory', this.rForm.get('productCategory').value);
        input.append('file', this.rForm.get('file').value);
        return input;
    };
    AddProductsComponent.prototype.addProducts = function (post) {
        var _this = this;
        var formModel = this.prepareSave();
        this.service.addProducts(formModel).subscribe(function (res) {
            console.log("Your blog is saved!::", res);
            if (res.code = 200) {
                alert(res.message);
                _this.toastr.success('product added successfully');
                _this.router.navigate(['/manage-products/manage-products']);
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], AddProductsComponent.prototype, "fileInput", void 0);
    AddProductsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-products',
            template: __webpack_require__(/*! ./add-products.component.html */ "./src/app/admin/components/manage-products/components/add-products/add-products.component.html"),
            styles: [__webpack_require__(/*! ./add-products.component.css */ "./src/app/admin/components/manage-products/components/add-products/add-products.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], AddProductsComponent);
    return AddProductsComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/manage-products/components/edit-products/edit-products.component.css":
/*!*******************************************************************************************************!*\
  !*** ./src/app/admin/components/manage-products/components/edit-products/edit-products.component.css ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/components/manage-products/components/edit-products/edit-products.component.html":
/*!********************************************************************************************************!*\
  !*** ./src/app/admin/components/manage-products/components/edit-products/edit-products.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.min.css\">\n\n<div class=\"container\">\n  <div *ngIf=\"!name; else forminfo\">\n    <form [formGroup]=\"rForm\" (ngSubmit)=\"editProducts(rForm.value)\">\n      <div class=\"form-container\">\n        <div class=\"row columns\">\n          <h1>Edit Products</h1>\n          <label style=\"font-size: 15px\">Name of product\n            <input type=\"text\" formControlName=\"productName\" class=\"form-control\">\n          </label>\n          <!-- <div class=\"alert\" *ngIf=\"!rForm.controls['name'].valid && rForm.controls['name'].touched\">{{ titleAlert }}</div> -->\n          <br>\n          <label style=\"font-size: 15px\">Description\n          </label>\n          <textarea class=\"form-control rounded-0\" id=\"exampleFormControlTextarea1\" rows=\"5\" formControlName=\"productDescription\"></textarea>\n  \n          <label style=\"font-size: 15px\">Cost of product\n            <input type=\"text\" formControlName=\"productCost\" class=\"form-control\">\n          </label>\n          <br>\n\n          <label style=\"font-size: 15px\">Product Category\n            <input type=\"text\" formControlName=\"productCategory\" class=\"form-control\">\n          </label>\n          <br>\n\n          <!-- <div class=\"alert\" *ngIf=\"!rForm.controls['description'].valid && rForm.controls['description'].touched\">You must specify a description that's between 30 and 500 characters.</div> -->\n          <input type=\"file\" id=\"file\" (change)=\"onFileChange($event)\" #fileInput>\n          <input type=\"submit\" class=\"button expanded\" value=\"Submit Form\" style=\"font-size: 15px\">\n        </div>\n      </div>\n    </form>\n  </div>\n</div>\n\n\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/admin/components/manage-products/components/edit-products/edit-products.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/admin/components/manage-products/components/edit-products/edit-products.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: EditProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProductsComponent", function() { return EditProductsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditProductsComponent = /** @class */ (function () {
    function EditProductsComponent(fb, route, service, router) {
        this.fb = fb;
        this.route = route;
        this.service = service;
        this.router = router;
        this.productName = '';
        this.productDescription = '';
        this.productCategory = '';
        this.rForm = fb.group({
            'productName': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'productDescription': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'productCost': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'productCategory': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'validate': '',
            file: null
        });
    }
    EditProductsComponent.prototype.ngOnInit = function () {
    };
    EditProductsComponent.prototype.onFileChange = function (event) {
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            this.rForm.get('file').setValue(file);
        }
    };
    EditProductsComponent.prototype.prepareSave = function () {
        var input = new FormData();
        input.append('productName', this.rForm.get('productName').value);
        input.append('productDescription', this.rForm.get('productDescription').value);
        input.append('productCost', this.rForm.get('productCost').value);
        input.append('productCategory', this.rForm.get('productCategory').value);
        input.append('file', this.rForm.get('file').value);
        return input;
    };
    EditProductsComponent.prototype.editProducts = function (data, id) {
        var _this = this;
        var formModel = this.prepareSave();
        this.route.params.subscribe(function (params) {
            _this.service.editProducts(params.id, formModel).subscribe(function (res) {
                console.log("Edit your blog!::", res);
                if (res.code = 200) {
                    alert(res);
                }
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], EditProductsComponent.prototype, "fileInput", void 0);
    EditProductsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-products',
            template: __webpack_require__(/*! ./edit-products.component.html */ "./src/app/admin/components/manage-products/components/edit-products/edit-products.component.html"),
            styles: [__webpack_require__(/*! ./edit-products.component.css */ "./src/app/admin/components/manage-products/components/edit-products/edit-products.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], EditProductsComponent);
    return EditProductsComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/manage-products/manage-products.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/admin/components/manage-products/manage-products.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".img{\n    height: 100px;\n    width: 100px    \n}\n.button{\n    margin-left: 5px\n}"

/***/ }),

/***/ "./src/app/admin/components/manage-products/manage-products.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/admin/components/manage-products/manage-products.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <app-navbar></app-navbar>\n<div class=\"container\">\n    <h1>Products Management</h1>\n    <div class=\"col-md-4\">\n        <button class=\"btn btn-primary\" [routerLink]=\"['/add-products']\" style=\"margin: 15px\">Add Products</button>\n    </div>\n    <br>\n    <table class=\"table table-striped\">\n      <thead>\n          <tr>\n              <th scope=\"col\">Image</th>\n              <th scope=\"col\">Product Name</th>\n              <th scope=\"col\">Product Description</th>\n              <th scope=\"col\">Product Cost</th>\n              <th scope=\"col\">Product Category</th>\n            </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor=\"let products of productslist\">\n            <td><img src=\"{{products.image}}\" alt=\"\" ng-style=\"{ width : image.width, height : image.height }\"></td>\n            <td>{{products.productName}}</td>\n          <td>{{products.productDescription}}</td>\n          <td>{{products.productCost}}</td>\n          <td>{{products.productCategory}}</td>\n            <td>\n                <button class=\"btn btn-primary btn-sm\" [routerLink]=\"['/edit-products',products._id]\">Edit</button>\n                <button class=\"btn btn-danger btn-sm\" style=\"margin-left: 5px\" (click)=\"deleteProducts(products._id)\">Delete</button>\n              </td>\n        </tr>\n      </tbody>\n    </table>\n  </div> \n<app-footer></app-footer>\n -->\n\n\n\n<html>\n    <head>\n <link rel=\"stylesheet\" type=\"text/css\" href=\"/node_modules/primeicons/primeicons.css\" />\n <link rel=\"stylesheet\" type=\"text/css\" href=\"/node_modules/primeng/resources/themes/nova-light/theme.css\" />\n <link rel=\"stylesheet\" type=\"text/css\" href=\"/node_modules/primeng/resources/primeng.min.css\" />\n <meta charset=\"utf-8\">\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\">\n  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\"></script>\n  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\"></script>\n</head>\n<body>\n        <app-navbar></app-navbar>\n\n  <h1>Products Management</h1>\n\n  <div class=\"col-md-4\">\n        <button class=\"btn btn-primary\" [routerLink]=\"['/add-products']\" style=\"margin: 15px\">Add Products</button>\n    </div>\n <p-table [value]=\"productslist\" [rows]=\"1\" [paginator]=\"true\">\n    <ng-template pTemplate=\"header\">\n        <tr>\n            <th scope=\"col\">Image</th>\n            <th scope=\"col\">Product Name</th>\n            <th scope=\"col\">Product Description</th>\n            <th scope=\"col\">Product Cost</th>\n            <th scope=\"col\">Product Category</th>\n            <th scope=\"col\">Action</th>\n\n        </tr>\n    </ng-template>\n    <ng-template pTemplate=\"body\" let-products>\n        <tr>\n            <td><img src=\"{{products.file}}\" alt=\"\" height=\"100px\" width=\"100px\"></td>\n            <td>{{products.productName}}</td>\n          <td>{{products.productDescription}}</td>\n          <td>{{products.productCost}}</td>\n          <td>{{products.productCategory}}</td>\n          <button pButton type=\"button\" label=\"Delete\" (click)=\"deleteProducts(products._id)\"></button>&nbsp;\n          <button pButton type=\"button\" label=\"Edit\" [routerLink]=\"['/edit-products',products._id]\"></button>\n\n        </tr>\n    </ng-template>\n</p-table>\n</body>\n</html>\n<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "./src/app/admin/components/manage-products/manage-products.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/admin/components/manage-products/manage-products.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ManageProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManageProductsComponent", function() { return ManageProductsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/user.service */ "./src/app/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ManageProductsComponent = /** @class */ (function () {
    function ManageProductsComponent(products, router) {
        this.products = products;
        this.router = router;
    }
    ManageProductsComponent.prototype.ngOnInit = function () {
        this.getDetails();
    };
    ManageProductsComponent.prototype.getDetails = function () {
        var _this = this;
        this.products.listProducts().subscribe(function (res) {
            _this.productslist = res;
            console.log("products", _this.productslist);
        });
    };
    ManageProductsComponent.prototype.deleteProducts = function (id) {
        this.products.deleteProducts(id).subscribe(function (res) {
            console.log('Deleted');
            // this.router.navigate(['blogs-listing/blogs-listing'])
            location.reload();
        });
        // this.router.navigate(['blogs-listing/blogs-listing'])
    };
    ManageProductsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manage-products',
            template: __webpack_require__(/*! ./manage-products.component.html */ "./src/app/admin/components/manage-products/manage-products.component.html"),
            styles: [__webpack_require__(/*! ./manage-products.component.css */ "./src/app/admin/components/manage-products/manage-products.component.css")]
        }),
        __metadata("design:paramtypes", [_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], ManageProductsComponent);
    return ManageProductsComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/manage-users/components/add-user/add-user.component.css":
/*!******************************************************************************************!*\
  !*** ./src/app/admin/components/manage-users/components/add-user/add-user.component.css ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/components/manage-users/components/add-user/add-user.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/admin/components/manage-users/components/add-user/add-user.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.min.css\">\n<div *ngIf=\"!name; else forminfo\">\n    <form [formGroup]=\"rForm\" (ngSubmit)=\"addUsers(rForm.value)\">\n      <div class=\"form-container\">\n        <div class=\"row columns\">\n          <h1>Add User</h1>\n          <label style=\"font-size: 15px\">Username\n            <input type=\"text\" formControlName=\"username\" class=\"form-control\">\n          </label>\n          <label style=\"font-size: 15px\">Password\n              <input type=\"text\" formControlName=\"password\" class=\"form-control\">\n            </label>\n            <label style=\"font-size: 15px\">Email\n                <input type=\"text\" formControlName=\"email\" class=\"form-control\">\n              </label>\n              <label style=\"font-size: 15px\">Role\n                  <input type=\"text\" formControlName=\"role\" class=\"form-control\">\n                </label>\n            <input type=\"submit\" class=\"button expanded\" value=\"Submit Form\" style=\"font-size: 15px\">\n      </div>\n    </div>\n  </form>\n</div> \n"

/***/ }),

/***/ "./src/app/admin/components/manage-users/components/add-user/add-user.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/admin/components/manage-users/components/add-user/add-user.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: AddUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUserComponent", function() { return AddUserComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddUserComponent = /** @class */ (function () {
    function AddUserComponent(fb, service, router) {
        this.fb = fb;
        this.service = service;
        this.router = router;
        this.password = '';
        this.username = '';
        this.email = '';
        this.imagesBlog = '';
        this.titleAlert = 'This field is required';
        this.loading = false;
        this.role = '';
        this.rForm = fb.group({
            'username': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'email': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'role': ["admin", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            'validate': '',
        });
    }
    AddUserComponent.prototype.ngOnInit = function () {
    };
    AddUserComponent.prototype.addUsers = function (post) {
        this.service.addUsers(post).subscribe(function (res) {
            console.log("User added!::", res);
            if (res.code = 200) {
                alert(res.message);
            }
        });
    };
    AddUserComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-user',
            template: __webpack_require__(/*! ./add-user.component.html */ "./src/app/admin/components/manage-users/components/add-user/add-user.component.html"),
            styles: [__webpack_require__(/*! ./add-user.component.css */ "./src/app/admin/components/manage-users/components/add-user/add-user.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AddUserComponent);
    return AddUserComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/manage-users/components/edit-users/edit-users.component.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/admin/components/manage-users/components/edit-users/edit-users.component.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/components/manage-users/components/edit-users/edit-users.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/admin/components/manage-users/components/edit-users/edit-users.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.min.css\">\n<div *ngIf=\"!name; else forminfo\">\n    <form [formGroup]=\"rForm\" (ngSubmit)=\"editUsers(rForm.value)\">\n      <div class=\"form-container\">\n        <div class=\"row columns\">\n          <h1>Add User</h1>\n          <label style=\"font-size: 15px\">Username\n            <input type=\"text\" formControlName=\"username\" class=\"form-control\">\n          </label>\n          <label style=\"font-size: 15px\">Password\n              <input type=\"text\" formControlName=\"password\" class=\"form-control\">\n            </label>\n            <label style=\"font-size: 15px\">Email\n                <input type=\"text\" formControlName=\"email\" class=\"form-control\">\n              </label>\n            <input type=\"submit\" class=\"button expanded\" value=\"Submit Form\" style=\"font-size: 15px\">\n      </div>\n    </div>\n  </form>\n</div> \n"

/***/ }),

/***/ "./src/app/admin/components/manage-users/components/edit-users/edit-users.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/admin/components/manage-users/components/edit-users/edit-users.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: EditUsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditUsersComponent", function() { return EditUsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditUsersComponent = /** @class */ (function () {
    // imagesBlog:string = '';
    // titleAlert:string = 'This field is required';
    // loading: boolean = false;
    // file:null;
    function EditUsersComponent(fb, route, service, router) {
        this.fb = fb;
        this.route = route;
        this.service = service;
        this.router = router;
        this.description = '';
        this.username = '';
        this.password = '';
        this.email = '';
        this.rForm = fb.group({
            'username': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'email': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'validate': '',
        });
    }
    EditUsersComponent.prototype.ngOnInit = function () {
    };
    EditUsersComponent.prototype.editUsers = function (data, id) {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.service.editUsers(params.id, _this.rForm.value).subscribe(function (res) {
                console.log("Edit your user!::", res);
                if (res.code = 200) {
                    alert(res);
                }
            });
        });
    };
    EditUsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-users',
            template: __webpack_require__(/*! ./edit-users.component.html */ "./src/app/admin/components/manage-users/components/edit-users/edit-users.component.html"),
            styles: [__webpack_require__(/*! ./edit-users.component.css */ "./src/app/admin/components/manage-users/components/edit-users/edit-users.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], EditUsersComponent);
    return EditUsersComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/manage-users/manage-users.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/admin/components/manage-users/manage-users.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/components/manage-users/manage-users.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/admin/components/manage-users/manage-users.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"col-md-10\">\n<h1>Users</h1>\n<div class=\"col-md-6\">\n    <button class=\"btn btn-primary\" [routerLink]=\"['/add-users']\">Add User</button>\n</div>\n<br>\n<table class=\"table table-striped table-dark\">\n  <thead>\n    <tr>\n      <th scope=\"col\">Username</th>\n      <th scope=\"col\">Password</th>\n      <th scope=\"col\">Email</th>\n      <th scope=\"col\">Role</th>\n      <th scope=\"col\">Action</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let users of userslist\">\n      <!-- <td><img src=\"{{blogs.image}}\" alt=\"\" height=\"10%\" width=\"10%\"></td> -->\n      <td>{{users.username}}</td>\n      <td>{{users.password}}</td>\n      <td>{{users.email}}</td>\n      <td>{{users.role}}</td>\n      <td><button class=\"btn btn-primary\" [routerLink]=\"['/edit-users',users._id]\">Edit</button><button class=\"btn btn-danger\" style=\"margin-left: 5px\" (click)=\"deleteUsers(users._id)\">Delete</button></td>\n   \n    </tr>\n \n  </tbody>\n</table>\n</div>\n<!-- <div class=\"container\">\n  <div class=\"col-md-6\">  \n    <h1>Users List</h1>\n  </div>\n  <div class=\"col-md-6\">\n    <button class=\"btn btn-primary\" [routerLink]=\"['/add-user/add-user']\">\n  </div>\n    <br>\n  <div class=\"col-md-11\">\n  <table class=\"table table-striped table-dark\">\n  <thead>\n    <tr>\n      <th scope=\"col\">User</th>\n      <th scope=\"col\">Description</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let blogs of blogslist\">\n      <td><img src=\"{{blogs.image}}\" alt=\"\" height=\"10%\" width=\"10%\"></td>\n      <td>{{blogs.name}}</td>\n      <td>{{blogs.description}}</td>\n      <td><button class=\"btn btn-primary\" [routerLink]=\"['/edit-user/edit-user',blogs._id]\">Edit</button>/<button class=\"btn btn-danger\" (click)=\"delete(blogs._id)\">Delete</button></td>   \n    </tr>\n \n  </tbody>\n</table>\n</div>\n</div> -->"

/***/ }),

/***/ "./src/app/admin/components/manage-users/manage-users.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/admin/components/manage-users/manage-users.component.ts ***!
  \*************************************************************************/
/*! exports provided: ManageUsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManageUsersComponent", function() { return ManageUsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/user.service */ "./src/app/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ManageUsersComponent = /** @class */ (function () {
    function ManageUsersComponent(users, router) {
        this.users = users;
        this.router = router;
    }
    ManageUsersComponent.prototype.ngOnInit = function () {
        this.getDetails();
    };
    ManageUsersComponent.prototype.getDetails = function () {
        var _this = this;
        this.users.listUsers().subscribe(function (res) {
            _this.userslist = res;
            console.log("users", _this.userslist);
        });
    };
    ManageUsersComponent.prototype.deleteUsers = function (id) {
        this.users.deleteUsers(id).subscribe(function (res) {
            console.log('Deleted');
            // this.router.navigate(['blogs-listing/blogs-listing'])
            location.reload();
        });
        // this.router.navigate(['blogs-listing/blogs-listing'])
    };
    ManageUsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manage-users',
            template: __webpack_require__(/*! ./manage-users.component.html */ "./src/app/admin/components/manage-users/manage-users.component.html"),
            styles: [__webpack_require__(/*! ./manage-users.component.css */ "./src/app/admin/components/manage-users/manage-users.component.css")]
        }),
        __metadata("design:paramtypes", [_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], ManageUsersComponent);
    return ManageUsersComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/navbar/navbar.component.css":
/*!**************************************************************!*\
  !*** ./src/app/admin/components/navbar/navbar.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/components/navbar/navbar.component.html":
/*!***************************************************************!*\
  !*** ./src/app/admin/components/navbar/navbar.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n  <title>Welcome User</title>\n  <meta charset=\"utf-8\">\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n  <style>\n    /* Remove the navbar's default rounded borders and increase the bottom margin */ \n    .navbar {\n      margin-bottom: 50px;\n      border-radius: 0;\n    }\n    \n    /* Remove the jumbotron's default bottom margin */ \n     .jumbotron {\n      margin-bottom: 0;\n    }\n    /* Logo size adjusting*/\n    /* .nav navbar-nav img{\n      width: 20%;\n      height: 25%\n    } */\n   \n    /* Add a gray background color and some padding to the footer */\n    footer {\n      background-color: #f2f2f2;\n      padding: 25px;\n    }\n  </style>\n</head>\n<body>\n<nav class=\"navbar navbar-inverse\">\n  <div class=\"container-fluid\">\n    <div class=\"navbar-header\">\n      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>                        \n      </button>\n      <!-- <a routerLink ='/logout'>Ecommerce</a> -->\n      <!-- <img src=\"../../../../assets/images/naruto-wallpaper-for-mobile-2.jpg\"> -->\n    </div>\n    <div class=\"collapse navbar-collapse\" id=\"myNavbar\">\n      <ul class=\"nav navbar-nav\">\n        <li><a href=\"/dashboard\">Today's deals!</a></li>\n        <!-- <li><a routerLink ='/profile'>Profile</a></li> -->\n        <li><a routerLink ='/reg-user-blogs/reg-user-blogs'>Manage-Blogs</a></li>\n        <li><a routerLink ='/manage-users/manage-users'>Manage-user</a></li>\n        <li><a routerLink ='/manage-products/manage-products'>Manage-products</a></li>\n        <li><a routerLink ='/contact-us/contact-us'>Contact-us</a></li>\n\n      </ul>\n      <ul class=\"nav navbar-nav navbar-right\">\n        <!-- <li><a routerLink=\"/account\"><span class=\"glyphicon glyphicon-user\"></span> Your Account</a></li> -->\n        <!-- <li><a routerLink=\"/cart\"><span class=\"glyphicon glyphicon-shopping-cart\"></span> Cart</a></li> -->\n        <li><a class=\"nav-link\" (click)=\"logout()\"><span class=\"glyphicon glyphicon-log-out\"></span> Logout</a></li>\n      </ul>\n    </div>\n  </div>\n</nav>\n</body>\n</html>\n"

/***/ }),

/***/ "./src/app/admin/components/navbar/navbar.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/admin/components/navbar/navbar.component.ts ***!
  \*************************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(router) {
        this.router = router;
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.logout = function () {
        localStorage.removeItem('token');
        this.router.navigate(['/']);
    };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/admin/components/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/admin/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/reg-user-blogs/components/blog-listing/blog-listing.component.css":
/*!****************************************************************************************************!*\
  !*** ./src/app/admin/components/reg-user-blogs/components/blog-listing/blog-listing.component.css ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/components/reg-user-blogs/components/blog-listing/blog-listing.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/admin/components/reg-user-blogs/components/blog-listing/blog-listing.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n  <div class=\"container\">\n    <h2>Blogs List</h2>   \n    <table class=\"table table-striped\">\n      <thead>\n        <tr>\n          <th>Blog Image</th>\n          <th>Blog name</th>\n          <th>Description</th>\n          <th>Action</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor=\"let blogs of blogslist\">\n            <td><img src=\"{{blogs.file}}\" alt=\"\" height=\"50px\" width=\"50px\"></td>\n            <td>{{blogs.name}}</td>\n            <td>{{blogs.description}}</td>\n            <td>\n                <button class=\"btn btn-primary btn-sm\" [routerLink]=\"['/edit-blogs/edit-blogs',blogs._id]\">Edit</button>&nbsp;\n                <button class=\"btn btn-danger btn-sm\" (click)=\"delete(blogs._id)\">Delete</button>\n              </td>\n        </tr>\n      </tbody>\n    </table>\n  </div> \n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/admin/components/reg-user-blogs/components/blog-listing/blog-listing.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/admin/components/reg-user-blogs/components/blog-listing/blog-listing.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: BlogListingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogListingComponent", function() { return BlogListingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/user.service */ "./src/app/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BlogListingComponent = /** @class */ (function () {
    function BlogListingComponent(blogs, router) {
        this.blogs = blogs;
        this.router = router;
    }
    BlogListingComponent.prototype.ngOnInit = function () {
        this.getDetails();
    };
    BlogListingComponent.prototype.getDetails = function () {
        var _this = this;
        this.blogs.listBlogs().subscribe(function (res) {
            _this.blogslist = res;
            console.log("blogs", _this.blogslist);
        });
    };
    BlogListingComponent.prototype.delete = function (id) {
        this.blogs.delete(id).subscribe(function (res) {
            console.log('Deleted');
            // this.router.navigate(['blogs-listing/blogs-listing'])
            location.reload();
        });
        // this.router.navigate(['blogs-listing/blogs-listing'])
    };
    BlogListingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-blog-listing',
            template: __webpack_require__(/*! ./blog-listing.component.html */ "./src/app/admin/components/reg-user-blogs/components/blog-listing/blog-listing.component.html"),
            styles: [__webpack_require__(/*! ./blog-listing.component.css */ "./src/app/admin/components/reg-user-blogs/components/blog-listing/blog-listing.component.css")]
        }),
        __metadata("design:paramtypes", [_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], BlogListingComponent);
    return BlogListingComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/reg-user-blogs/components/edit-blogs/edit-blogs.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/admin/components/reg-user-blogs/components/edit-blogs/edit-blogs.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/components/reg-user-blogs/components/edit-blogs/edit-blogs.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/admin/components/reg-user-blogs/components/edit-blogs/edit-blogs.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <!DOCTYPE html>\n<html lang=\"en\">\n<head>\n  <title>Bootstrap Example</title>\n  <meta charset=\"utf-8\">\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\">\n  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\"></script>\n  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\"></script>\n</head>\n<body>\n\n<div class=\"container\">\n  <h2>Edit your blog</h2>\n  <form action=\"/action_page.php\">\n    <div class=\"form-group\">\n      <label for=\"comment\">Comment:</label>\n      <textarea class=\"form-control\" rows=\"5\" id=\"comment\" name=\"text\"></textarea>\n    </div>\n    <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n  </form>\n</div>\n\n</body>\n</html> -->\n\n<app-navbar></app-navbar>\n<html>\n  <head>\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.min.css\">\n</head>\n<body>\n  <div class=\"col-md-2\"></div>\n<div class=\"col-md-10\">\n<div *ngIf=\"!name; else forminfo\">\n  <form [formGroup]=\"rForm\" (ngSubmit)=\"editBlogs(rForm.value)\">\n    <div class=\"form-container\">\n      <div class=\"row columns\">\n        <h1>My Blogs</h1>\n        <label style=\"font-size: 15px\">Name of Blog\n          <input type=\"text\" formControlName=\"name\" class=\"form-control\">\n        </label>\n        <div class=\"alert\" *ngIf=\"!rForm.controls['name'].valid && rForm.controls['name'].touched\">{{ titleAlert }}</div>\n        <br>\n        \n        <label style=\"font-size: 15px\">Description\n          <!-- <input type=\"text\" formControlName=\"description\" class=\"form-control\"> -->\n        </label>  \n        <textarea class=\"form-control rounded-0\" id=\"exampleFormControlTextarea1\" rows=\"10\" formControlName=\"description\"></textarea>\n\n        <div class=\"alert\" *ngIf=\"!rForm.controls['description'].valid && rForm.controls['description'].touched\">You must specify a description that's between 30 and 500 characters.</div>\n        <input type=\"file\" id=\"file\" (change)=\"onFileChange($event)\" #fileInput>\n\n        <!-- <div class=\"form-group\">\n            <label for=\"exampleFormControlTextarea1\">Large textarea</label>\n            <textarea class=\"form-control rounded-0\" id=\"exampleFormControlTextarea1\" rows=\"10\"></textarea>\n        </div> -->\n        \n        <!-- <label>Email\n          <input type=\"email\" formControlName=\"email\" required email=\"true\">\n        </label>\n        <div *ngIf=\"officialEmail.errors\"> \n          <div *ngIf=\"officialEmail.errors.required\">\n              Correct Email required.\n          </div> \t\t   \n          <div *ngIf=\"officialEmail.errors.pattern\">\n              Email not valid.\n          </div> \n       </div>   -->\n       <input type=\"submit\" class=\"button expanded\" value=\"Submit Form\" style=\"font-size: 15px\" [disabled]=\"!rForm.valid\">\n      </div>\n    </div>\n  </form>\n</div>\n</div>\n</body>\n</html>\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/admin/components/reg-user-blogs/components/edit-blogs/edit-blogs.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/admin/components/reg-user-blogs/components/edit-blogs/edit-blogs.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: EditBlogsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditBlogsComponent", function() { return EditBlogsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditBlogsComponent = /** @class */ (function () {
    function EditBlogsComponent(fb, route, service, router) {
        this.fb = fb;
        this.route = route;
        this.service = service;
        this.router = router;
        this.description = '';
        this.name = '';
        this.titleAlert = 'This field is required';
        this.rForm = fb.group({
            'name': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'description': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(30), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(9000)])],
            'validate': '',
            file: null
        });
    }
    EditBlogsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.rForm.get('validate').valueChanges.subscribe(function (validate) {
            if (validate == '1') {
                _this.rForm.get('name').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3)]);
                _this.titleAlert = 'You need to specify at least 3 characters';
            }
            else {
                _this.rForm.get('name').setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required);
            }
            _this.rForm.get('name').updateValueAndValidity();
        });
    };
    EditBlogsComponent.prototype.onFileChange = function (event) {
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            this.rForm.get('file').setValue(file);
        }
    };
    EditBlogsComponent.prototype.prepareSave = function () {
        var input = new FormData();
        input.append('name', this.rForm.get('name').value);
        input.append('description', this.rForm.get('description').value);
        input.append('file', this.rForm.get('file').value);
        return input;
    };
    EditBlogsComponent.prototype.editBlogs = function (data, id) {
        var _this = this;
        var formModel = this.prepareSave();
        this.route.params.subscribe(function (params) {
            _this.service.editBlogs(params.id, formModel).subscribe(function (res) {
                console.log("Edit your blog!::", res);
                if (res.code = 200) {
                    alert(res);
                    _this.router.navigate['/blogs-listing/blogs-listing'];
                }
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], EditBlogsComponent.prototype, "fileInput", void 0);
    EditBlogsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-blogs',
            template: __webpack_require__(/*! ./edit-blogs.component.html */ "./src/app/admin/components/reg-user-blogs/components/edit-blogs/edit-blogs.component.html"),
            styles: [__webpack_require__(/*! ./edit-blogs.component.css */ "./src/app/admin/components/reg-user-blogs/components/edit-blogs/edit-blogs.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], EditBlogsComponent);
    return EditBlogsComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/reg-user-blogs/components/write-blogs/write-blogs.component.css":
/*!**************************************************************************************************!*\
  !*** ./src/app/admin/components/reg-user-blogs/components/write-blogs/write-blogs.component.css ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* @import url('https://fonts.googleapis.com/css?family=Muli:400,700');\n\nbody {\n    background:#e8e8e8;\n    font-family: 'Muli';\n}\n.form-container {\n    display:block;\n    width:90%;\n    padding:2em;\n    margin: 2em auto;\n    background:#fff;\n}\n\n.alert {\n    background: #f2edda;\n    padding: 7px;\n    font-size: .9em;\n    margin-bottom: 20px;\n    display: inline-block;\n    animation: 2s alertAnim forwards;\n}\n\n.button {\n    margin-top: 3rem;\n}\nh1 {\n    margin-bottom: 2rem;\n    font-weight:bold;\n    font-family:'Muli';\n    font-size: 2em;\n}\n\n\n@keyframes alertAnim {\n    from {\n        opacity:0;\n        transform: translateY(-20px);\n    }\n    to {\n        opacity:1;\n        transform: translateY(0);\n    }\n} */\n\n/* @import url('https://fonts.googleapis.com/css?family=Muli:400,700');\n\nbody {\n    background:#e8e8e8;\n    font-family: 'Muli';\n}\n.form-container {\n    display:block;\n    width:90%;\n    padding:2em;\n    margin: 2em auto;\n    background:#fff;\n}\n\n.alert {\n    background: #f2edda;\n    padding: 7px;\n    font-size: .9em;\n    margin-bottom: 20px;\n    display: inline-block;\n    animation: 2s alertAnim forwards;\n}\n\n.button {\n    margin-top: 3rem;\n}\nh1 {\n    margin-bottom: 2rem;\n    font-weight:bold;\n    font-family:'Muli';\n    font-size: 2em;\n}\n\n\n@keyframes alertAnim {\n    from {\n        opacity:0;\n        transform: translateY(-20px);\n    }\n    to {\n        opacity:1;\n        transform: translateY(0);\n    }\n} */\n\n.form-container {\n    display:block;\n    width:90%;\n    padding:2em;\n    margin: 2em auto;\n    background:#fff;\n}\n\n.alert {\n    background: #f2edda;\n    padding: 7px;\n    font-size: .9em;\n    margin-bottom: 20px;\n    display: inline-block;\n    -webkit-animation: 2s alertAnim forwards;\n            animation: 2s alertAnim forwards;\n}\n\n.button {\n    margin-top: 3rem;\n}\n\nh1 {\n    margin-bottom: 2rem;\n    font-weight:bold;\n    font-family:'Muli';\n    font-size: 2em;\n}\n\n@-webkit-keyframes alertAnim {\n    from {\n        opacity:0;\n        -webkit-transform: translateY(-20px);\n                transform: translateY(-20px);\n    }\n    to {\n        opacity:1;\n        -webkit-transform: translateY(0);\n                transform: translateY(0);\n    }\n}\n\n@keyframes alertAnim {\n    from {\n        opacity:0;\n        -webkit-transform: translateY(-20px);\n                transform: translateY(-20px);\n    }\n    to {\n        opacity:1;\n        -webkit-transform: translateY(0);\n                transform: translateY(0);\n    }\n}\n"

/***/ }),

/***/ "./src/app/admin/components/reg-user-blogs/components/write-blogs/write-blogs.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/admin/components/reg-user-blogs/components/write-blogs/write-blogs.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/css/foundation.min.css\">\n\n\n<div *ngIf=\"!name; else forminfo\">\n  <form [formGroup]=\"rForm\" (ngSubmit)=\"addBlogs(rForm.value)\">\n    <div class=\"form-container\">\n      <div class=\"row columns\">\n        <h1>My Blogs</h1>\n        <label style=\"font-size: 15px\">Name of Blog\n          <input type=\"text\" formControlName=\"name\" class=\"form-control\">\n        </label>\n        <div class=\"alert\" *ngIf=\"!rForm.controls['name'].valid && rForm.controls['name'].touched\">{{ titleAlert }}</div>\n        <br>\n        <p-editor formControlName=\"description\" [style]=\"{'height':'320px'}\"></p-editor>\n        <!-- <label style=\"font-size: 15px\">Description\n        </label>  \n        <textarea class=\"form-control rounded-0\" id=\"exampleFormControlTextarea1\" rows=\"10\" formControlName=\"description\"></textarea>\n\n        <div class=\"alert\" *ngIf=\"!rForm.controls['description'].valid && rForm.controls['description'].touched\">You must specify a description that's between 30 and 500 characters.</div> -->\n        <input type=\"file\" id=\"file\" (change)=\"onFileChange($event)\" #fileInput>\n\n     \n       <input type=\"submit\" class=\"button expanded\" value=\"Submit Form\" style=\"font-size: 15px\" [disabled]=\"!rForm.valid\">\n      </div>\n    </div>\n  </form>\n</div>\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/admin/components/reg-user-blogs/components/write-blogs/write-blogs.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/admin/components/reg-user-blogs/components/write-blogs/write-blogs.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: WriteBlogsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WriteBlogsComponent", function() { return WriteBlogsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var WriteBlogsComponent = /** @class */ (function () {
    // fileToUpload: File = null;
    // public uploader:FileUploader = new FileUploader({});
    function WriteBlogsComponent(fb, service, router, toastr) {
        this.fb = fb;
        this.service = service;
        this.router = router;
        this.toastr = toastr;
        this.text1 = '<div>Hello World!</div><div>PrimeNG <b>Editor</b> Rocks</div><div><br></div>';
        this.description = '';
        this.name = '';
        this.imagesBlog = '';
        this.titleAlert = 'This field is required';
        this.loading = false;
        this.rForm = fb.group({
            'name': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            'description': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(30), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(9000)])],
            'validate': '',
            // 'imagesBlog':[null,Validators.required],
            file: null
            // 'file':[null,Validators.required]
        });
        // console.log(this.uploader)
    }
    // addPost(post) {
    //   this.description = post.description;
    //   this.name = post.name;
    // }
    WriteBlogsComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.rForm= new FormGroup({
        //   'username': new FormControl,                //username
        //   'password':new FormControl                 //password
        // })
        this.rForm.get('validate').valueChanges.subscribe(function (validate) {
            if (validate == '1') {
                _this.rForm.get('name').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3)]);
                _this.titleAlert = 'You need to specify at least 3 characters';
            }
            else {
                _this.rForm.get('name').setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required);
            }
            _this.rForm.get('name').updateValueAndValidity();
        });
    };
    WriteBlogsComponent.prototype.onFileChange = function (event) {
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            this.rForm.get('file').setValue(file);
        }
    };
    WriteBlogsComponent.prototype.prepareSave = function () {
        var input = new FormData();
        input.append('name', this.rForm.get('name').value);
        input.append('description', this.rForm.get('description').value);
        input.append('file', this.rForm.get('file').value);
        return input;
    };
    WriteBlogsComponent.prototype.addBlogs = function (post) {
        var _this = this;
        var formModel = this.prepareSave();
        this.service.addBlogs(formModel).subscribe(function (res) {
            console.log("Your blog is saved!::", res);
            if (res.code = 200) {
                alert(res.message);
                _this.toastr.success('blog added successfully');
                _this.router.navigate(['/blogs-listing/blogs-listing']);
            }
            // else{
            //   this.toastr.error('please add all fields');
            // }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], WriteBlogsComponent.prototype, "fileInput", void 0);
    WriteBlogsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-write-blogs',
            template: __webpack_require__(/*! ./write-blogs.component.html */ "./src/app/admin/components/reg-user-blogs/components/write-blogs/write-blogs.component.html"),
            styles: [__webpack_require__(/*! ./write-blogs.component.css */ "./src/app/admin/components/reg-user-blogs/components/write-blogs/write-blogs.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], WriteBlogsComponent);
    return WriteBlogsComponent;
}());



/***/ }),

/***/ "./src/app/admin/components/reg-user-blogs/reg-user-blogs.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/admin/components/reg-user-blogs/reg-user-blogs.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".wrapper {\n    display: flex;\n    width: 100%;\n}\n\n#sidebar {\n    width: 250px;\n    position: fixed;\n    top: 0;\n    left: 0;\n    height: 100vh;\n    z-index: 999;\n    background: #7386D5;\n    color: #fff;\n    transition: all 0.3s;\n}\n\n.main h3{\n    margin-top: 0%\n}\n"

/***/ }),

/***/ "./src/app/admin/components/reg-user-blogs/reg-user-blogs.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/admin/components/reg-user-blogs/reg-user-blogs.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<!DOCTYPE html>\n<html>\n<head>\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n<style>\nbody {\n    font-family: \"Lato\", sans-serif;\n}\n\n.sidenav {\n    height: 100%;\n    width: 160px;\n    position: fixed;\n    z-index: 1;\n    top: 0;\n    left: 0;\n    background-color: #111;\n    overflow-x: hidden;\n    padding-top: 20px;\n}\n\n.sidenav a {\n    padding: 6px 8px 6px 16px;\n    text-decoration: none;\n    font-size: 25px;\n    color: #818181;\n    display: block;\n}\n\n.sidenav a:hover {\n    color: #f1f1f1;\n}\n\n.main {\n    margin-left: 160px; /* Same as the width of the sidenav */\n    font-size: 28px; /* Increased text to enable scrolling */\n    padding: 0px 10px;\n}\n\n@media screen and (max-height: 450px) {\n    .sidenav {padding-top: 15px;}\n    .sidenav li a {font-size: 18px;}\n    .sidenav h3 {font-size: 18px;}\n\n}\n</style>\n</head>\n<body>\n\n<div class=\"sidenav\">\n  <h3>Welcome</h3>\n    <!-- <li><a routerLink =''>Edit Blog</a></li> -->\n    <li><a routerLink ='/write-blogs/write-blogs'>Write Blog</a></li>\n    <li><a routerLink ='/blogs-listing/blogs-listing'>Blogs</a></li>\n\n    \n</div>\n\n<div class=\"main\">\n    <div class=\"container\">\n    <div class=\"col-md-8\"><h1>Manage blogs</h1></div>\n    <div class=\"col-md-4\"><h3>Welcome admin!</h3></div>\n    </div>\n    <img src=\"../../../../assets/images/Blog-Writing.jpg\">\n  <!-- <h2>Blog on life</h2>\n  <p>This sidebar is of full height (100%) and always shown.</p>\n  <p>Scroll down the page to see the result.</p>\n  <p>Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eum. Affert laboramus repudiandae nec et. Inciderint efficiantur his ad. Eum no molestiae voluptatibus.</p>\n  <p>Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eum. Affert laboramus repudiandae nec et. Inciderint efficiantur his ad. Eum no molestiae voluptatibus.</p>\n  <p>Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eum. Affert laboramus repudiandae nec et. Inciderint efficiantur his ad. Eum no molestiae voluptatibus.</p>\n  <p>Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eum. Affert laboramus repudiandae nec et. Inciderint efficiantur his ad. Eum no molestiae voluptatibus.</p> -->\n</div>\n     \n</body>\n</html> \n\n<app-footer></app-footer> "

/***/ }),

/***/ "./src/app/admin/components/reg-user-blogs/reg-user-blogs.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/admin/components/reg-user-blogs/reg-user-blogs.component.ts ***!
  \*****************************************************************************/
/*! exports provided: RegUserBlogsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegUserBlogsComponent", function() { return RegUserBlogsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RegUserBlogsComponent = /** @class */ (function () {
    function RegUserBlogsComponent() {
    }
    RegUserBlogsComponent.prototype.ngOnInit = function () {
    };
    RegUserBlogsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reg-user-blogs',
            template: __webpack_require__(/*! ./reg-user-blogs.component.html */ "./src/app/admin/components/reg-user-blogs/reg-user-blogs.component.html"),
            styles: [__webpack_require__(/*! ./reg-user-blogs.component.css */ "./src/app/admin/components/reg-user-blogs/reg-user-blogs.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], RegUserBlogsComponent);
    return RegUserBlogsComponent;
}());



/***/ })

}]);
//# sourceMappingURL=admin-admin-module.js.map